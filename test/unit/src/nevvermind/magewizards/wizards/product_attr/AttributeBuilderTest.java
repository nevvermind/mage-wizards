/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.netbeans.junit.NbTestCase;

/**
 * We're testing Exception the old-school way. Dunno why, but the @Test(expected = IllegalArgumentException.class)
 * didn't work. Bummer.
 */
public class AttributeBuilderTest extends NbTestCase
{
    private AttributeBuilder attrBuilder;
    
    public AttributeBuilderTest(String name)
    {
        super(name);
    }
    
    @Before
    @Override
    protected void setUp() throws Exception
    {
        attrBuilder = new AttributeBuilder();
    }
    
    @After
    @Override
    public void tearDown()
    {
        attrBuilder = null;
    }

    @Test
    public void testInvalidFilterableValue1()
    {
        try {
            attrBuilder.setFilterable("filter this!");
            fail("Should throw");
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
            fail("Should've failed with IllegalArgumentException only");
        }
    }
    
    @Test
    public void testInvalidFilterableValue2()
    {
        try {
            attrBuilder.setFilterable("     ");
            fail("Should throw");
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
            fail("Should've failed with IllegalArgumentException only");
        }
    }
    
    @Test
    public void testInvalidFilterableValue3()
    {
        try {
            attrBuilder.setFilterable("no"); // close, but the value is "No", not "no"
            fail("Should throw");
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
            fail("Should've failed with IllegalArgumentException only");
        }
    }
}
