/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions;

import nevvermind.magewizards.ui.swing.TrueFalseCombo;
import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.BackendTypeCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.specific.IsFilterableCombo;
import org.junit.Test;
import static org.junit.Assert.*;

public class LayeredNavIndexableAttributeTest extends AbstractSuggestionTestCase
{
    public LayeredNavIndexableAttributeTest(String name)
    {
        super(name);
    }

    public Service getSuggestion()
    {
        return new LayeredNavIndexableAttribute();
    }
    
    /**
     * Prepare a valid Layered Nav Select AttributeBuilder
     * 
     * @param withResults
     * @return 
     */
    private AttributeBuilder getValidSelectLayeredNavAttribute(Boolean withResults)
    {
        attrBuilder = new AttributeBuilder();
        attrBuilder.setVisibleInAdvancedSearch(TrueFalseCombo.TRUE);
        attrBuilder.setFilterableInSearch(TrueFalseCombo.TRUE);
        attrBuilder.setBackendType(BackendTypeCombo.TYPE_INT);
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_SELECT);
        attrBuilder.setFilterable(withResults ? IsFilterableCombo.VAL_YES_RESULTS : IsFilterableCombo.VAL_YES_NO_RESULTS);
        
        return attrBuilder;
    }
    
    /**
     * Prepare a valid Layered Nav Multi-select AttributeBuilder
     * 
     * @param withResults
     * @return 
     */
    private AttributeBuilder getValidMultiselectLayeredNavAttribute(Boolean withResults)
    {
        attrBuilder = getValidSelectLayeredNavAttribute(withResults);
        attrBuilder.setBackendType(BackendTypeCombo.TYPE_VARCHAR);
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_MULTISELECT);
        return attrBuilder;
    }

    @Test
    public void testSuggestionShowsOnValidSettings()
    {
        attrBuilder = getValidSelectLayeredNavAttribute(true);
        assertTrue(suggestion.mustShow(attrBuilder));
        
        attrBuilder = getValidSelectLayeredNavAttribute(false);
        assertTrue(suggestion.mustShow(attrBuilder));
        
        attrBuilder = getValidMultiselectLayeredNavAttribute(true);
        assertTrue(suggestion.mustShow(attrBuilder));
        
        attrBuilder = getValidMultiselectLayeredNavAttribute(false);
        assertTrue(suggestion.mustShow(attrBuilder));
    }

    @Test
    public void testSuggestionDoesNotShowsOnInvalidSettings()
    {
        attrBuilder = getValidSelectLayeredNavAttribute(true);
        assertTrue("Attribute should be valid here", suggestion.mustShow(attrBuilder));
        attrBuilder.setFilterable(IsFilterableCombo.VAL_NO);
        assertFalse("Layered Nav is invalid with filterable = no", suggestion.mustShow(attrBuilder));
        
        attrBuilder = getValidSelectLayeredNavAttribute(true);
        assertTrue("Attribute should be valid here", suggestion.mustShow(attrBuilder));
        attrBuilder.setVisibleInAdvancedSearch(TrueFalseCombo.FALSE);
        assertFalse("Layered Nav is invalid with visible_in_adv_search = no", suggestion.mustShow(attrBuilder));
        
        attrBuilder = getValidSelectLayeredNavAttribute(true);
        assertTrue("Attribute should be valid here", suggestion.mustShow(attrBuilder));
        attrBuilder.setFilterableInSearch(TrueFalseCombo.FALSE);
        assertFalse("Layered Nav is invalid with filterable_in_search = no", suggestion.mustShow(attrBuilder));
    }

    @Test
    public void testSuggestionDoesNotShowsOnInvalidInputs()
    {
        attrBuilder = getValidSelectLayeredNavAttribute(true); // frontend input type = select
        assertTrue("Attribute should be valid here", suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_BOOLEAN);
        assertFalse(suggestion.mustShow(attrBuilder));
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_DATE);
        assertFalse(suggestion.mustShow(attrBuilder));
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_MEDIA_IMAGE);
        assertFalse(suggestion.mustShow(attrBuilder));
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_MULTISELECT);
        assertFalse("A Multiselect and frontend input = int should fail", suggestion.mustShow(attrBuilder));
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_PRICE);
        assertFalse(suggestion.mustShow(attrBuilder));
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_TEXT);
        assertFalse(suggestion.mustShow(attrBuilder));
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_TEXTAREA);
        assertFalse(suggestion.mustShow(attrBuilder));
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_WEEE);
        assertFalse(suggestion.mustShow(attrBuilder));
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_SELECT);
        assertTrue(suggestion.mustShow(attrBuilder)); // back to a valid frontend input with "int"
    }
}
