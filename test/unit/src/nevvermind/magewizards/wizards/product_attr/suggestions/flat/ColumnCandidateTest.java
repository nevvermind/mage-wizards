/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions.flat;

import nevvermind.magewizards.ui.swing.TrueFalseCombo;
import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.BackendTypeCombo;
import nevvermind.magewizards.wizards.product_attr.suggestions.AbstractSuggestionTestCase;
import nevvermind.magewizards.wizards.product_attr.suggestions.Service;
import org.junit.Test;

public class ColumnCandidateTest extends AbstractSuggestionTestCase
{
    public ColumnCandidateTest(String name)
    {
        super(name);
    }
    
    @Override
    protected Service getSuggestion()
    {
        return new ColumnCandidate();
    }
    
    @Test
    public void testSuggestionIsShownOnAllValidConfiguration()
    {
        assertFalse(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setUsedForPromoRules(TrueFalseCombo.TRUE);
        attrBuilder.setUsedForSortBy(TrueFalseCombo.TRUE);
        attrBuilder.setUsedInProductListing(TrueFalseCombo.TRUE);
        attrBuilder.setBackendType(BackendTypeCombo.TYPE_STATIC);      
        
        assertTrue(suggestion.mustShow(attrBuilder));
    }
    
    @Test
    public void testSuggestionIsShownOnSomeValidConfiguration()
    {
        assertFalse(suggestion.mustShow(attrBuilder)); // fresh invalid attr builder from parent class
        
        AttributeBuilder partialBuilder1 = new AttributeBuilder();
        partialBuilder1.setUsedForPromoRules(TrueFalseCombo.TRUE);
        assertTrue(suggestion.mustShow(partialBuilder1));
        
        AttributeBuilder partialBuilder2 = new AttributeBuilder();
        partialBuilder2.setUsedForSortBy(TrueFalseCombo.TRUE);
        assertTrue(suggestion.mustShow(partialBuilder2));
        
        AttributeBuilder partialBuilder3 = new AttributeBuilder();
        partialBuilder3.setUsedInProductListing(TrueFalseCombo.TRUE);
        assertTrue(suggestion.mustShow(partialBuilder3));
        
        AttributeBuilder partialBuilder4 = new AttributeBuilder();
        partialBuilder4.setBackendType(BackendTypeCombo.TYPE_STATIC);
        assertTrue(suggestion.mustShow(partialBuilder4));
    }
    
    @Test
    public void testSuggestionNotShownOnSomeValidConfiguration()
    {
        assertFalse(suggestion.mustShow(attrBuilder)); // fresh invalid attr builder from parent class
        
        AttributeBuilder partialBuilder1 = new AttributeBuilder();
        partialBuilder1.setUsedForPromoRules(TrueFalseCombo.FALSE);
        assertFalse(suggestion.mustShow(partialBuilder1));
        
        AttributeBuilder partialBuilder2 = new AttributeBuilder();
        partialBuilder2.setUsedForSortBy(TrueFalseCombo.FALSE);
        assertFalse(suggestion.mustShow(partialBuilder2));
        
        AttributeBuilder partialBuilder3 = new AttributeBuilder();
        partialBuilder3.setUsedInProductListing(TrueFalseCombo.FALSE);
        assertFalse(suggestion.mustShow(partialBuilder3));
        
        AttributeBuilder partialBuilder4 = new AttributeBuilder();
        partialBuilder4.setBackendType(BackendTypeCombo.TYPE_INT);
        assertFalse("Should fail if the backend type is something other than \"static\"", suggestion.mustShow(partialBuilder4));
    }
}
