/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions;

import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import org.junit.Test;
import static org.junit.Assert.*;

public class NoSourceImplTest extends AbstractSuggestionTestCase
{
    public NoSourceImplTest(String name)
    {
        super(name);
    }

    @Override
    protected Service getSuggestion()
    {
        return new NoSourceImpl();
    }    

    @Test
    public void testSuggestionShownOnMultiselectOrSelect()
    {
        attrBuilder.setSourceModel("");
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_SELECT);
        assertTrue(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_MULTISELECT);
        assertTrue(suggestion.mustShow(attrBuilder));
    }
    
    @Test
    public void testSuggestionNotShownOnOtherThanMultiselectOrSelect()
    {
        attrBuilder.setSourceModel("valid/source      ");
        assertEquals("valid/source", attrBuilder.getSourceModel()); // little test in between
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_BOOLEAN);
        assertFalse(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_TEXT);
        assertFalse(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_TEXTAREA);
        assertFalse(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_DATE);
        assertFalse(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_MEDIA_IMAGE);
        assertFalse(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_WEEE);
        assertFalse(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_PRICE);
        assertFalse(suggestion.mustShow(attrBuilder));
    }

    @Test
    public void testSuggestionNotShownOnEmptyInputType()
    {
        attrBuilder.setFrontendInput("");
        assertFalse(suggestion.mustShow(attrBuilder));
    }

    @Test
    public void testSuggestionNotShownOnCorrectSettings()
    {
        attrBuilder.setSourceModel("some_source/model");
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_SELECT);
        assertFalse(suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_MULTISELECT);
        assertFalse(suggestion.mustShow(attrBuilder));
    }
    
    @Test
    public void testSpaces()
    {
        attrBuilder.setSourceModel("         ");
        assertEquals("", attrBuilder.getSourceModel());
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_SELECT);
        assertTrue("Source model is invalid (just space). This should fail.", suggestion.mustShow(attrBuilder));
        
        attrBuilder.setFrontendInput(FrontendInputCombo.TYPE_MULTISELECT);
        assertTrue("Source model is invalid (just space). This should fail.", suggestion.mustShow(attrBuilder));        
    }
}
