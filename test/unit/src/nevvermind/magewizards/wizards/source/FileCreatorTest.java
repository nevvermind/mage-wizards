/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.source;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import nevvermind.magewizards.wizards.TestHelper;
import nevvermind.magewizards.wizards.source.ui.SourceTypeCombo;
import org.junit.After;
import org.junit.Test;
import org.netbeans.junit.NbTestCase;

public class FileCreatorTest extends NbTestCase
{
    
    public FileCreatorTest(String name)
    {
        super(name);
    }
    
    @After
    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
        FileCreator.INSTANCE.reset();
    }
    
    @Test
    public void testClassNameChangesDependingOnSourceType()
    {
        FileCreator.INSTANCE.setModuleNs("ns");
        FileCreator.INSTANCE.setModuleName("name");
        FileCreator.INSTANCE.setSourceFileName("filename");
        
        FileCreator.INSTANCE.setSourceType(SourceTypeCombo.TYPE_CONFIG);
        assertEquals(FileCreator.INSTANCE.getClassName(), "Ns_Name_Model_System_Config_Source_Filename");
        
        FileCreator.INSTANCE.setSourceType(SourceTypeCombo.TYPE_ATTRIBUTE);
        assertEquals(FileCreator.INSTANCE.getClassName(), "Ns_Name_Model_Attribute_Source_Filename");
        
        FileCreator.INSTANCE.setSourceType(SourceTypeCombo.TYPE_ATTRIBUTE);
        assertEquals(FileCreator.INSTANCE.getClassName(), "Ns_Name_Model_Attribute_Source_Filename");
    }
    
    @Test
    public void testIsCorrectSourceIdentified() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        Method isAttrSourceMethod = FileCreator.INSTANCE.getClass().getDeclaredMethod("isAttributeSource");
        Method isConfigSourceMethod = FileCreator.INSTANCE.getClass().getDeclaredMethod("isConfigSource");
        
        FileCreator.INSTANCE.setSourceType(SourceTypeCombo.TYPE_ATTRIBUTE);
        assertFalse((Boolean) TestHelper.invokeRestricted(FileCreator.INSTANCE, isConfigSourceMethod));
        assertTrue((Boolean) TestHelper.invokeRestricted(FileCreator.INSTANCE, isAttrSourceMethod));
        
        FileCreator.INSTANCE.setSourceType(SourceTypeCombo.TYPE_CONFIG);
        assertTrue((Boolean) TestHelper.invokeRestricted(FileCreator.INSTANCE, isConfigSourceMethod));
        assertFalse((Boolean) TestHelper.invokeRestricted(FileCreator.INSTANCE, isAttrSourceMethod));
        
        FileCreator.INSTANCE.setSourceType(SourceTypeCombo.TYPE_BOTH);
        assertTrue((Boolean) TestHelper.invokeRestricted(FileCreator.INSTANCE, isConfigSourceMethod));
        assertTrue((Boolean) TestHelper.invokeRestricted(FileCreator.INSTANCE, isAttrSourceMethod));
    }
    
    public void testModulePath() throws NoSuchMethodException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
    {
        Method getModulePathMethod = FileCreator.INSTANCE.getClass().getDeclaredMethod("getModulePath");
        
        FileCreator.INSTANCE.setModuleName("name");
        FileCreator.INSTANCE.setModuleNs("ns");
        assertEquals((String)  TestHelper.invokeRestricted(FileCreator.INSTANCE, getModulePathMethod), "/app/code/local/Ns/Name");
        
        FileCreator.INSTANCE.setModuleName("NAME");
        FileCreator.INSTANCE.setModuleNs("NS");
        assertEquals((String) TestHelper.invokeRestricted(FileCreator.INSTANCE, getModulePathMethod), "/app/code/local/NS/NAME");
        
        FileCreator.INSTANCE.setModuleName("camelCaseNAME");
        FileCreator.INSTANCE.setModuleNs("camelCaseNS");
        assertEquals((String) TestHelper.invokeRestricted(FileCreator.INSTANCE, getModulePathMethod), "/app/code/local/CamelCaseNS/CamelCaseNAME");
    }
}
