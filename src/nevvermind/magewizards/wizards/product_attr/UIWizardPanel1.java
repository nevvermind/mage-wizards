/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr;

import javax.swing.event.ChangeListener;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.SortOrderTextField;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;

public class UIWizardPanel1 
    implements WizardDescriptor.Panel<WizardDescriptor>, 
               WizardDescriptor.ValidatingPanel<WizardDescriptor>
{
    private boolean isValid = true;

    private UIVisualPanel1 component;
    private final AttributeBuilder attrBuilder;

    public UIWizardPanel1(AttributeBuilder attrBuilder)
    {
        this.attrBuilder = attrBuilder;        
    }
    
    @Override
    public UIVisualPanel1 getComponent()
    {
        if (component == null) {
            component = new UIVisualPanel1();
        }
        return component;
    }

    @Override
    public HelpCtx getHelp()
    {
         return new HelpCtx("nevvermind.magewizards.wizards.product_attr");
    }

    @Override
    public boolean isValid()
    {
        // If it is always OK to press Next or Finish, then:
        return isValid;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
    }

    @Override
    public void addChangeListener(ChangeListener l)
    {
        }        

    @Override
    public void removeChangeListener(ChangeListener l)
    {
        }        
    
    @Override
    public void readSettings(WizardDescriptor wiz)
    {
        // use wiz.getProperty to retrieve previous panel state
    }

    @Override
    public void storeSettings(WizardDescriptor wiz)
    {
        UIVisualPanel1 panel1 = getComponent();
        attrBuilder.setAttributeCode(panel1.attributeCodeTextField.getText());
        attrBuilder.setBackendType((String) panel1.backendTypeCombo.getSelectedItem());
        attrBuilder.setBackendTable(panel1.backendTableTextField.getText());
        attrBuilder.setFrontendModel(panel1.frontendModelTextField.getText());
        attrBuilder.setFrontendInput((String) panel1.frontendInputCombo.getSelectedItem());
        attrBuilder.setFrontendLabel(panel1.frontendLabelTextField.getText());
        attrBuilder.setFrontendClass((String) panel1.frontendClassCombo.getSelectedItem());
        attrBuilder.setGroup(panel1.groupTextField.getText());
        attrBuilder.setRequired((String) panel1.isRequiredCombo.getSelectedItem());
        attrBuilder.setUserDefined((String) panel1.isUserDefinedCombo.getSelectedItem());
        attrBuilder.setDefaultValue(panel1.defaultValueTextField.getText());
        attrBuilder.setUnique((String) panel1.isUniqueCombo.getSelectedItem());
        attrBuilder.setGlobal((String) panel1.globalCombo.getSelectedItem());
        attrBuilder.setBackendModel((String) panel1.backendModelCombo.getSelectedItem());
        attrBuilder.setNote(panel1.noteTextArea.getText());
        attrBuilder.setSourceModel((String) panel1.sourceModelCombo.getSelectedItem());
        attrBuilder.setSortOrder(panel1.sortOrderTextField.getText());
    }

    @Override
    public void validate() throws WizardValidationException
    {
        UIVisualPanel1 panel1 = getComponent();
        
        String attrCode = panel1.attributeCodeTextField.getText().trim();
        
        if (attrCode.equals("")) {
            throw new WizardValidationException(null, "Please provide an attribute code.", null);
        }
        
        if (!AttributeBuilder.isPhpConstant(attrCode) && !attrCode.matches("^([a-z]([a-z_0-9])*){1,254}$")) {
            throw new WizardValidationException(null, "Invalid attribute code. It must start with a letter and must contain only a-z, 0-9 and _ characters.", null);
        }
        
        String frontendLabel = panel1.frontendLabelTextField.getText().trim();
        if (frontendLabel.equals("")) {
            throw new WizardValidationException(null, "Please provide a Frontend Label.", null);
        }
        
        if (!((SortOrderTextField) panel1.sortOrderTextField).isValidText()) {
            throw new WizardValidationException(null, "Invalid Sort Order. Use only numbers.", null);
        }
        
        panel1.sortOrderTextField.setText(panel1.sortOrderTextField.getText().trim());
        panel1.frontendLabelTextField.setText(panel1.frontendLabelTextField.getText().trim());
        panel1.attributeCodeTextField.setText(panel1.attributeCodeTextField.getText().trim());
        
        isValid = true;
    }
}
