/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.*;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.specific.*;

public class AttributeBuilder 
{
    private static final String TPL = 
"/* @var $installer Mage_Catalog_Model_Resource_Setup */\n" +
"// $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');\n" +
"\n" +
"// $installer->startSetup();\n\n" +
"$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, %s, array(\n" +
"%s" +
"));\n\n" +
"// $installer->endSetup();\n"
;
    
    private final static String INDT = "    ";
    private final static HashMap<String, String> all = new LinkedHashMap<String, String>();
    private final static HashMap<String, String> GLOBAL_MAP = new HashMap<String, String>();
    static 
    {
        GLOBAL_MAP.put(GlobalCombo.SCOPE_GLOBAL, "Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL");
        GLOBAL_MAP.put(GlobalCombo.SCOPE_STORE, "Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE");
        GLOBAL_MAP.put(GlobalCombo.SCOPE_WEBSITE, "Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE");
    }
    
    private String attributeCode;
    private String backendType;
    private String backendTable;
    private String frontendModel;
    private String frontendInput;
    private String frontendLabel;
    private String frontendClass;
    private String group;
    private String required;
    private String userDefined;
    private String defaultValue;
    private String unique;
    private String global;
    private String backendModel;
    private String note;
    private String sourceModel;
    private String sortOrder;
    
    // --------- Specific
    private String visible;
    private String searchable;
    private String filterable;
    private String comparable;
    private String visibleOnFront;
    private String wysiwygEnabled;
    private String htmlAllowedOnFront;
    private String visibleInAdvancedSearch;
    private String filterableInSearch;
    private String usedInProductListing;
    private String usedForSortBy;
    private String configurable;
    private String usedForPromoRules;
    private String position;
    private String frontendInputRenderer;
    private Object[] applyTo;
    
    public static HashMap<String, String> getAll()
    {
        return all;
    }
    
    public static void reset()
    {
        all.clear();
    }
    
    public static boolean isPhpConstant(String value)
    {
        return value.matches("\\w+::\\w+");
    }
    
    public static String escapeSingleQuotes(String value)
    {
        return value.replaceAll("'", Matcher.quoteReplacement("\\") + "'");
    }
    
    protected String getPhpArrayElement(String key, String value)
    {
        return String.format("%s => %s,", key, value);
    }
    
    protected String getPhpArrayElementString(String key, String value)
    {
        if (!AttributeBuilder.isPhpConstant(value)) {
            value = "'" + AttributeBuilder.escapeSingleQuotes(value) + "'";
        }
        return getPhpArrayElement("'" + key + "'", value);
    }
    
    protected String getPhpArrayElementBool(String key, String value)
    {
        if (value.matches("true") || value.matches("false")) {
            return getPhpArrayElement("'" + key + "'", value);
        }
        throw new IllegalArgumentException("Args should be either string \"false\" or \"true\".");
    }    
    
    @Override
    public String toString()
    {
        StringBuilder attrStringBuilder = new StringBuilder();
        
        Iterator<Map.Entry<String, String>> it = getAll().entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry<String, String> attribute = it.next();
            attrStringBuilder.append(attribute.getValue()).append("\n");
        }
        
        String attrCode = getAttributeCode().trim();
        if (!AttributeBuilder.isPhpConstant(getAttributeCode())) {
            attrCode = "'" + AttributeBuilder.escapeSingleQuotes(attrCode) + "'";
        }
        return String.format(TPL, attrCode, attrStringBuilder.toString());
    }

    public String getAttributeCode()
    {
        return attributeCode;
    }

    public void setAttributeCode(String attributeCode)
    {
        this.attributeCode = attributeCode.trim();
    }

    public String getBackendType()
    {
        return backendType;
    }

    public void setBackendType(String backendType)
    {
        if (StringUtils.isNotBlank(backendType)) {
            getAll().put(BackendTypeCombo.CODE, INDT + getPhpArrayElementString(BackendTypeCombo.CODE, backendType));
        }
        this.backendType = backendType;
    }

    public String getBackendTable()
    {
        return backendTable;
    }

    public void setBackendTable(String backendTable)
    {
        if (StringUtils.isNotBlank(backendTable)) {
            getAll().put(BackendTableTextField.CODE, INDT + getPhpArrayElementString(BackendTableTextField.CODE, backendTable));
        }        
        this.backendTable = backendTable;
    }

    public String getFrontendModel()
    {
        return frontendModel;
    }

    public void setFrontendModel(String frontendModel)
    {
        if (StringUtils.isNotBlank(frontendModel)) {
            getAll().put(FrontendModelTextField.CODE, INDT + getPhpArrayElementString(FrontendModelTextField.CODE, frontendModel));
        }    
        this.frontendModel = frontendModel;
    }

    public String getFrontendInput()
    {
        return frontendInput;
    }

    public void setFrontendInput(String frontendInput)
    {
        if (StringUtils.isNotBlank(frontendInput)) {
            getAll().put(FrontendInputCombo.CODE, INDT + getPhpArrayElementString(FrontendInputCombo.CODE, frontendInput));
        }
        this.frontendInput = frontendInput;
    }

    public String getFrontendLabel()
    {
        return frontendLabel;
    }

    public void setFrontendLabel(String frontendLabel)
    {
        if (StringUtils.isNotBlank(frontendLabel)) {
            getAll().put(FrontendLabelTextField.CODE, INDT + getPhpArrayElementString(FrontendLabelTextField.CODE, frontendLabel));
        }
        this.frontendLabel = frontendLabel;
    }

    public String getFrontendClass()
    {
        return frontendClass;
    }

    public void setFrontendClass(String frontendClass)
    {
        if (StringUtils.isNotBlank(frontendClass)) {
            getAll().put(FrontendClassTextField.CODE, INDT + getPhpArrayElementString(FrontendClassTextField.CODE, frontendClass));
        }
        this.frontendClass = frontendClass;
    }

    public String getGroup()
    {
        return group;
    }

    public void setGroup(String group)
    {
        if (StringUtils.isNotBlank(group)) {
            getAll().put(GroupTextField.CODE, INDT + getPhpArrayElementString(GroupTextField.CODE, group));
        }
        this.group = group;
    }

    public String getRequired()
    {
        return required;
    }

    public void setRequired(String required)
    {
        if (StringUtils.isNotBlank(required)) {
            getAll().put(IsRequiredCombo.CODE, INDT + getPhpArrayElementBool(IsRequiredCombo.CODE, required));
        }
        this.required = required;
    }

    public String getUserDefined()
    {
        return userDefined;
    }

    public void setUserDefined(String userDefined)
    {
        if (StringUtils.isNotBlank(userDefined)) {
            getAll().put(IsUserDefinedCombo.CODE, INDT + getPhpArrayElementBool(IsUserDefinedCombo.CODE, userDefined));
        }
        this.userDefined = userDefined;
    }

    public String getDefaultValue()
    {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue)
    {
        if (StringUtils.isNotBlank(defaultValue)) {
            getAll().put(DefaultValueTextField.CODE, INDT + getPhpArrayElementString(DefaultValueTextField.CODE, defaultValue));
        }
        this.defaultValue = defaultValue;
    }

    public String getUnique()
    {
        return unique;
    }

    public void setUnique(String unique)
    {
        if (StringUtils.isNotBlank(unique)) {
            getAll().put(IsUniqueCombo.CODE, INDT + getPhpArrayElementBool(IsUniqueCombo.CODE, unique));
        }
        this.unique = unique;
    }

    public String getGlobal()
    {
        return global;
    }

    public void setGlobal(String global)
    {
        if (StringUtils.isNotBlank(global)) {
            getAll().put(GlobalCombo.CODE, INDT + getPhpArrayElementString(GlobalCombo.CODE, GLOBAL_MAP.get(global)));
        }
        this.global = global;
    }

    public String getBackendModel()
    {
        return backendModel;        
    }

    public void setBackendModel(String backendModel)
    {
        if (StringUtils.isNotBlank(backendModel)) {
            getAll().put(BackendModelCombo.CODE, INDT + getPhpArrayElementString(BackendModelCombo.CODE, backendModel));
        }
        this.backendModel = backendModel;
    }

    public String getNote()
    {
        if (note != null) {
            return note.trim();
        }
        return note;
    }

    public void setNote(String note)
    {
        if (StringUtils.isNotBlank(note)) {
            getAll().put(NoteTextArea.CODE, INDT + getPhpArrayElementString(NoteTextArea.CODE, note.replaceAll("\n", "")));
        }
        this.note = note;
    }

    public String getVisible()
    {
        return visible;
    }

    public void setVisible(String visible)
    {
        if (StringUtils.isNotBlank(visible)) {
            getAll().put(IsVisibleCombo.CODE, INDT + getPhpArrayElementBool(IsVisibleCombo.CODE, visible));
        }
        this.visible = visible;
    }

    public String getSearchable()
    {
        return searchable;
    }

    public void setSearchable(String searchable)
    {
        if (StringUtils.isNotBlank(searchable)) {
            getAll().put(IsSearchableCombo.CODE, INDT + getPhpArrayElementBool(IsSearchableCombo.CODE, searchable));
        }
        this.searchable = searchable;
    }

    public String getFilterable()
    {
        return filterable;
    }

    public void setFilterable(String filterable)
    {
        if (!IsFilterableCombo.VALUE_MAP.containsKey(filterable)) {
            throw new IllegalArgumentException("Invalid \"is_filterable\" key provided: " + filterable + ". See IsFilterableCombo.VALUE_MAP keys.");
        }
        
        if (StringUtils.isNotBlank(filterable)) {
            getAll().put(IsFilterableCombo.CODE, INDT + getPhpArrayElementString(IsFilterableCombo.CODE, IsFilterableCombo.VALUE_MAP.get(filterable)));
        }
        
        this.filterable = filterable;
    }

    public String getComparable()
    {
        return comparable;
    }

    public void setComparable(String comparable)
    {
        if (StringUtils.isNotBlank(comparable)) {
            getAll().put(IsComparableCombo.CODE, INDT + getPhpArrayElementBool(IsComparableCombo.CODE, comparable));
        }
        this.comparable = comparable;
    }

    public String getVisibleOnFront()
    {
        return visibleOnFront;
    }

    public void setVisibleOnFront(String visibleOnFront)
    {
        if (StringUtils.isNotBlank(visibleOnFront)) {
            getAll().put(IsVisibleOnFrontCombo.CODE, INDT + getPhpArrayElementBool(IsVisibleOnFrontCombo.CODE, visibleOnFront));
        }
        this.visibleOnFront = visibleOnFront;
    }

    public String getWysiwygEnabled()
    {
        return wysiwygEnabled;
    }

    public void setWysiwygEnabled(String wysiwygEnabled)
    {
        if (StringUtils.isNotBlank(wysiwygEnabled)) {
            getAll().put(IsWysiwygEnabledCombo.CODE, INDT + getPhpArrayElementBool(IsWysiwygEnabledCombo.CODE, wysiwygEnabled));
        }
        this.wysiwygEnabled = wysiwygEnabled;
    }

    public String getHtmlAllowedOnFront()
    {
        return htmlAllowedOnFront;
    }

    public void setHtmlAllowedOnFront(String htmlAllowedOnFront)
    {
        if (StringUtils.isNotBlank(htmlAllowedOnFront)) {
            getAll().put(IsHtmlAllowedOnFrontCombo.CODE, INDT + getPhpArrayElementBool(IsHtmlAllowedOnFrontCombo.CODE, htmlAllowedOnFront));
        }
        this.htmlAllowedOnFront = htmlAllowedOnFront;
    }

    public String getVisibleInAdvancedSearch()
    {
        return visibleInAdvancedSearch;
    }

    public void setVisibleInAdvancedSearch(String visibleInAdvancedSearch)
    {
        if (StringUtils.isNotBlank(visibleInAdvancedSearch)) {
            getAll().put(IsVisibleInAdvancedSearchCombo.CODE, INDT + getPhpArrayElementBool(IsVisibleInAdvancedSearchCombo.CODE, visibleInAdvancedSearch));
        }
        this.visibleInAdvancedSearch = visibleInAdvancedSearch;
    }

    public String getFilterableInSearch()
    {
        return filterableInSearch;
    }

    public void setFilterableInSearch(String filterableInSearch)
    {
        if (StringUtils.isNotBlank(filterableInSearch)) {
            getAll().put(IsFilterableInSearchCombo.CODE, INDT + getPhpArrayElementBool(IsFilterableInSearchCombo.CODE, filterableInSearch));
        }
        this.filterableInSearch = filterableInSearch;
    }

    public String getUsedInProductListing()
    {
        return usedInProductListing;
    }

    public void setUsedInProductListing(String usedInProductListing)
    {
        if (StringUtils.isNotBlank(usedInProductListing)) {
            getAll().put(UsedInProductListingCombo.CODE, INDT + getPhpArrayElementBool(UsedInProductListingCombo.CODE, usedInProductListing));
        }
        this.usedInProductListing = usedInProductListing;
    }

    public String getUsedForSortBy()
    {
        return usedForSortBy;
    }

    public void setUsedForSortBy(String usedForSortBy)
    {
        if (StringUtils.isNotBlank(usedForSortBy)) {
            getAll().put(UsedForSortByCombo.CODE, INDT + getPhpArrayElementBool(UsedForSortByCombo.CODE, usedForSortBy));
        }
        this.usedForSortBy = usedForSortBy;
    }

    public String getConfigurable()
    {
        return configurable;
    }

    public void setConfigurable(String configurable)
    {
        if (StringUtils.isNotBlank(configurable)) {
            getAll().put(IsConfigurableCombo.CODE, INDT + getPhpArrayElementBool(IsConfigurableCombo.CODE, configurable));
        }
        this.configurable = configurable;
    }

    public String getUsedForPromoRules()
    {
        return usedForPromoRules;
    }

    public void setUsedForPromoRules(String usedForPromoRules)
    {
        if (StringUtils.isNotBlank(usedForPromoRules)) {
            getAll().put(IsUsedForPromoRulesCombo.CODE, INDT + getPhpArrayElementBool(IsUsedForPromoRulesCombo.CODE, usedForPromoRules));
        }
        this.usedForPromoRules = usedForPromoRules;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        if (StringUtils.isNotBlank(position)) {
            getAll().put(PositionTextField.CODE, INDT + getPhpArrayElementString(PositionTextField.CODE, position));
        }
        this.position = position;
    }

    public String getFrontendInputRenderer()
    {
        return frontendInputRenderer;
    }

    public void setFrontendInputRenderer(String frontendInputRenderer)
    {
        if (StringUtils.isNotBlank(frontendInputRenderer)) {
            getAll().put(FrontendInputRendererCombo.CODE, INDT + getPhpArrayElementString(FrontendInputRendererCombo.CODE, frontendInputRenderer));
        }
        this.frontendInputRenderer = frontendInputRenderer;
    }

    public Object[] getApplyTo()
    {
        return applyTo;
    }

    public void setApplyTo(Object[] applyTo)
    {
        if (applyTo.length != ApplyToList.VALUES.length) {
            
            String[] applyToTemp = new String[applyTo.length]; // don't change the original array
            
            for (int i = 0; i < applyToTemp.length; i++) {
                applyToTemp[i] = (String) applyTo[i];
            }
            
            getAll().put(ApplyToList.CODE, INDT + getPhpArrayElementString(ApplyToList.CODE, StringUtils.join(applyToTemp, ",")));
        } else {
            getAll().remove(ApplyToList.CODE);
        }
        
        this.applyTo = applyTo;
    }
    
    public String getSourceModel()
    {
        if (sourceModel != null) {
            return sourceModel.trim();
        }
        return sourceModel;
    }

    public void setSourceModel(String sourceModel)
    {
        if (StringUtils.isNotBlank(sourceModel)) {
            getAll().put(SourceModelCombo.CODE, INDT + getPhpArrayElementString(SourceModelCombo.CODE, sourceModel));
        }        
        this.sourceModel = sourceModel;
    }

    public String getSortOrder()
    {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder)
    {
        if (StringUtils.isNotBlank(sortOrder)) {
            getAll().put(SortOrderTextField.CODE, INDT + getPhpArrayElementString(SortOrderTextField.CODE, sortOrder));
        }         
        
        this.sortOrder = sortOrder;
    }
}
