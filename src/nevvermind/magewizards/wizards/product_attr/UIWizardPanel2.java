/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr;

import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;

public class UIWizardPanel2 
    implements WizardDescriptor.Panel<WizardDescriptor>,
               WizardDescriptor.ValidatingPanel<WizardDescriptor>
{

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private UIVisualPanel2 component;
    private final AttributeBuilder attrBuilder;
    
    public UIWizardPanel2(AttributeBuilder attrBuilder)
    {
        this.attrBuilder = attrBuilder;
    }    

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public UIVisualPanel2 getComponent()
    {
        if (component == null) {
            component = new UIVisualPanel2();
        }
        return component;
    }

    @Override
    public HelpCtx getHelp()
    {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx("help.key.here");
    }

    @Override
    public boolean isValid()
    {
        // If it is always OK to press Next or Finish, then:
        return true;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
    }

    @Override
    public void addChangeListener(ChangeListener l)
    {
    }

    @Override
    public void removeChangeListener(ChangeListener l)
    {
    }

    @Override
    public void readSettings(WizardDescriptor wiz)
    {
        // use wiz.getProperty to retrieve previous panel state
    }

    @Override
    public void storeSettings(WizardDescriptor wiz)
    {
        UIVisualPanel2 panel2 = getComponent();
        attrBuilder.setVisible((String) panel2.isVisibleCombo.getSelectedItem());
        attrBuilder.setSearchable((String) panel2.isSearchableCombo.getSelectedItem());
        attrBuilder.setFilterable((String) panel2.isFilterableCombo.getSelectedItem());
        attrBuilder.setComparable((String) panel2.isComparableCombo.getSelectedItem());
        attrBuilder.setVisibleOnFront((String) panel2.isVisibleOnFrontCombo.getSelectedItem());
        attrBuilder.setWysiwygEnabled((String) panel2.isWysiwygCombo.getSelectedItem());
        attrBuilder.setHtmlAllowedOnFront((String) panel2.isHtmlAllowedOnFrontCombo.getSelectedItem());
        attrBuilder.setVisibleInAdvancedSearch((String) panel2.isVisibleInAdvancedSearchCombo.getSelectedItem());
        attrBuilder.setFilterableInSearch((String) panel2.isFilterableInSearchCombo.getSelectedItem());
        attrBuilder.setUsedInProductListing((String) panel2.usedInProductListingCombo.getSelectedItem());
        attrBuilder.setUsedForSortBy((String) panel2.usedForSortByCombo.getSelectedItem());
        attrBuilder.setConfigurable((String) panel2.isConfigurableCombo.getSelectedItem());
        attrBuilder.setUsedForPromoRules((String) panel2.isUsedForPromoRulesCombo.getSelectedItem());
        attrBuilder.setPosition(panel2.positionTextField.getText());
        attrBuilder.setFrontendInputRenderer((String) panel2.frontendInputRendererCombo.getSelectedItem());
        attrBuilder.setApplyTo(panel2.applyToList.getSelectedValues());
    }

    @Override
    public void validate() throws WizardValidationException
    {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
