/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr;

import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import nevvermind.magewizards.wizards.product_attr.preselect.ServiceAbstract;

public final class UIWizardIterator 
    implements WizardDescriptor.Iterator<WizardDescriptor>,
               WizardDescriptor.InstantiatingIterator<WizardDescriptor>
{
    public final static Logger LOG = Logger.getLogger(UIVisualPanel1.class.getName());
    
    private int index;

    private List<WizardDescriptor.Panel<WizardDescriptor>> panels;

    private List<WizardDescriptor.Panel<WizardDescriptor>> getPanels()
    {
        if (panels == null) {
            panels = new ArrayList<WizardDescriptor.Panel<WizardDescriptor>>();
            
            AttributeBuilder attrBuilder = new AttributeBuilder();
            
            panels.add(new UIWizardPanel1(attrBuilder));
            panels.add(new UIWizardPanel2(attrBuilder));
            panels.add(new UIWizardPanel3(attrBuilder));
            
            String[] steps = new String[panels.size()];
            for (int i = 0; i < panels.size(); i++) {
                Component c = panels.get(i).getComponent();
                // Default step name to component name of panel.
                steps[i] = c.getName();
                if (c instanceof JComponent) { // assume Swing components
                    JComponent jc = (JComponent) c;
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_SELECTED_INDEX, i);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DATA, steps);
                    jc.putClientProperty(WizardDescriptor.PROP_AUTO_WIZARD_STYLE, true);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DISPLAYED, true);
                    jc.putClientProperty(WizardDescriptor.PROP_CONTENT_NUMBERED, true);
//                    jc.putClientProperty(WizardDescriptor.PROP_AUTO_WIZARD_STYLE, false);
                }
            }
            
            // fire pre-selection
            ((UIWizardPanel1) panels.get(0)).getComponent().preDefinedConfigsCombo.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e)
                {
                    ServiceAbstract preselect = ((ServiceAbstract) e.getItem());
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        LOG.info("Triggering Pre-Selection");
                        preselect.preselect(panels);
                    } else {
                        LOG.info("Triggering Deselect Pre-Selection");
                        preselect.deselect(panels);
                    }
                }
            });            
        }
        return panels;
    }

    @Override
    public WizardDescriptor.Panel<WizardDescriptor> current()
    {
        return getPanels().get(index);
    }

    @Override
    public String name()
    {
        return index + 1 + ". from " + getPanels().size();
    }

    @Override
    public boolean hasNext()
    {
        return index < getPanels().size() - 1;
    }

    @Override
    public boolean hasPrevious()
    {
        return index > 0;
    }

    @Override
    public void nextPanel()
    {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        
        index++;
    }

    @Override
    public void previousPanel()
    {
        if (!hasPrevious()) {
            throw new NoSuchElementException();
        }
        
        index--;
    }

    // If nothing unusual changes in the middle of the wizard, simply:
    @Override
    public void addChangeListener(ChangeListener l)
    {
    }

    @Override
    public void removeChangeListener(ChangeListener l)
    {
    }
    // If something changes dynamically (besides moving between panels), e.g.
    // the number of panels changes in response to user input, then use
    // ChangeSupport to implement add/removeChangeListener and call fireChange
    // when needed

    @Override
    public Set instantiate() throws IOException
    {
        return Collections.EMPTY_SET;
    }

    @Override
    public void initialize(WizardDescriptor wizard)
    {}

    @Override
    public void uninitialize(WizardDescriptor wizard)
    {
        AttributeBuilder.reset();
        panels = null;
    }
}
