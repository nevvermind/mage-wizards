/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.attribute.attribute.specific;

import java.util.HashMap;
import java.util.Map;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IAttributeConfig;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IDefaultable;

public class IsFilterableCombo extends javax.swing.JComboBox<String> implements IAttributeConfig, IDefaultable
{
    public static final String COL  = "is_filterable";
    public static final String CODE = "filterable";
    
    public final static String VAL_NO             = "No";
    public final static String VAL_YES_RESULTS    = "Yes (with results)";
    public final static String VAL_YES_NO_RESULTS = "Yes (no results)";
    
    public final static String CODE_NO             = "0";
    public final static String CODE_YES_RESULTS    = "1";
    public final static String CODE_YES_NO_RESULTS = "2";
    
    public final static String[] VALUES = { VAL_NO, VAL_YES_RESULTS, VAL_YES_NO_RESULTS };
    
    public final static Map<String, String> VALUE_MAP = new HashMap<String, String>();
    static {
        VALUE_MAP.put(VAL_NO, CODE_NO);
        VALUE_MAP.put(VAL_YES_RESULTS, CODE_YES_RESULTS);
        VALUE_MAP.put(VAL_YES_NO_RESULTS, CODE_YES_NO_RESULTS);
    }

    public IsFilterableCombo()
    {
        super(VALUES);
        setDefault();
    }
    
    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public final void setDefault()
    {
        this.setSelectedItem(VAL_NO);
    }
}
