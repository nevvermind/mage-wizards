/**
 * A Source Model must extend Mage_Eav_Model_Entity_Attribute_Source_Abstract or implement Mage_Eav_Model_Entity_Attribute_Source_Interface
 */
package nevvermind.magewizards.wizards.product_attr.attribute.attribute.base;

import javax.swing.JComboBox;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IAttributeConfig;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IDefaultable;

public class SourceModelCombo extends JComboBox<String> implements IAttributeConfig, IDefaultable
{
    public static final String COL  = "source_model";
    public static final String CODE = "source";
    
    public static final String TYPE_BOOLEAN = "eav/entity_attribute_source_boolean";
    public static final String TYPE_CONFIG  = "eav/entity_attribute_source_config";
    public static final String TYPE_COUNTRY = "customer/address_attribute_source_country";
    public static final String TYPE_REGION  = "customer/address_attribute_source_region";
    public static final String TYPE_STORE   = "customer/customer_attribute_source_store";
    public static final String TYPE_WEBSITE = "customer/customer_attribute_source_website";
    public static final String TYPE_TABLE   = "eav/entity_attribute_source_table";
    
    private static final String[] VALUES = {
        TYPE_BOOLEAN,
        TYPE_CONFIG,
        TYPE_COUNTRY,
        TYPE_REGION,
        TYPE_STORE,
        TYPE_WEBSITE,
        TYPE_TABLE
    };
    
    public SourceModelCombo()
    {
        super(VALUES);
        setDefault();
    }    

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public final void setDefault()
    {
        this.setSelectedIndex(-1);
    }
}
