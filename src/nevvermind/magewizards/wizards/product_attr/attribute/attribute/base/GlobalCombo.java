/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.attribute.attribute.base;

import javax.swing.JComboBox;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IAttributeConfig;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IDefaultable;

/**
 * class Mage_Catalog_Model_Resource_Eav_Attribute extends Mage_Eav_Model_Entity_Attribute
   {
    const SCOPE_STORE   = 0;
    const SCOPE_GLOBAL  = 1;
    const SCOPE_WEBSITE = 2;
 * 
 * @author nev
 */
public class GlobalCombo extends JComboBox<String> implements IAttributeConfig, IDefaultable
{
    public static final String COL  = "is_global";
    public static final String CODE = "global";    
    
    public static final String SCOPE_STORE   = "store";
    public static final String SCOPE_GLOBAL  = "global";
    public static final String SCOPE_WEBSITE = "website";
    
    private static final String[] VALUES = { SCOPE_STORE, SCOPE_GLOBAL, SCOPE_WEBSITE };
    
    public GlobalCombo()
    {
        super(VALUES);
        setDefault();
    }    

    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public final void setDefault()
    {
        this.setSelectedItem(SCOPE_GLOBAL);
    }
}
