/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.attribute.attribute.base;

import javax.swing.JComboBox;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IAttributeConfig;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IDefaultable;

public class FrontendInputCombo extends JComboBox<String> implements IAttributeConfig, IDefaultable
{
    public static final String COL  = "frontend_input";
    public static final String CODE = "input";
    
    public static final String TYPE_TEXT        = "text";
    public static final String TYPE_TEXTAREA    = "textarea";
    public static final String TYPE_DATE        = "date";
    public static final String TYPE_BOOLEAN     = "boolean";
    public static final String TYPE_MULTISELECT = "multiselect";
    public static final String TYPE_SELECT      = "select";
    public static final String TYPE_PRICE       = "price";
    public static final String TYPE_MEDIA_IMAGE = "media_image";
    public static final String TYPE_WEEE        = "weee";
    
    public static final String[] VALUES = new String[] {
        TYPE_TEXT, 
        TYPE_TEXTAREA, 
        TYPE_DATE, 
        TYPE_BOOLEAN, 
        TYPE_MULTISELECT, 
        TYPE_SELECT, 
        TYPE_PRICE, 
        TYPE_MEDIA_IMAGE, 
        TYPE_WEEE, 
    };
    
    public FrontendInputCombo()
    {
        super(VALUES);
        setDefault();
    }
    
    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public final void setDefault()
    {
        this.setSelectedItem(TYPE_TEXT);
    }
}
