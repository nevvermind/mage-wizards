/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.attribute.attribute.base;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IAttributeConfig;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IDefaultable;

public class BackendModelCombo extends JComboBox<String> implements IAttributeConfig, IDefaultable
{
    public static final String COL  = "backend_model";
    public static final String CODE = "backend";
    
    public static final String TYPE_ARRAY      = "eav/entity_attribute_backend_array";
    public static final String TYPE_DATETIME   = "eav/entity_attribute_backend_datetime";
    public static final String TYPE_SERIALIZED = "eav/entity_attribute_backend_serialized";
    
    public static final String[] VALUES = { TYPE_ARRAY, TYPE_DATETIME, TYPE_SERIALIZED };
    
    public BackendModelCombo()
    {
        super(VALUES);
        this.setDefault();
    }
    
    @Override
    public String getColumnName()
    {
        return COL;
    }

    @Override
    public String getCode()
    {
        return CODE;
    }

    @Override
    public final void setDefault()
    {
        this.setSelectedIndex(-1);
    }
    
    public final void resetValues()
    {
        this.setModel(new DefaultComboBoxModel<String>(VALUES));
    }
}
