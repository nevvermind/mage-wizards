/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr;

import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.wizards.product_attr.preselect.DefaultImpl;
import nevvermind.magewizards.wizards.product_attr.preselect.ServiceAbstract;
import org.openide.util.Lookup;

public class PreSelectionsCombo extends JComboBox<ServiceAbstract>
{
    private static final Collection<? extends ServiceAbstract> PRESELECTION_IMPLS = Lookup.getDefault().lookupAll(ServiceAbstract.class);
    
    private static final Logger LOG = Logger.getLogger(PreSelectionsCombo.class.getName());
            
    public PreSelectionsCombo()
    {
        super();
        
        this.addItem(new DefaultImpl()); // default first
        setSelectedIndex(0); // select first
        
        for (ServiceAbstract impl : PRESELECTION_IMPLS) {
            if (StringUtils.isNotBlank(impl.getLabel())) {
                this.addItem(impl);
                LOG.log(Level.FINE, "Pre-selection implementation added ({0}).", impl.getLabel());
            } else {
                LOG.log(Level.FINE, "Pre-selection implementation was discarded because of invalid label: {0}", impl.toString());
            }
        }
        
    }    
}
