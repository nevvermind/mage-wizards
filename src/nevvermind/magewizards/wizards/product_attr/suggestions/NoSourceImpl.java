/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions;

import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service=Service.class)
public class NoSourceImpl implements Service 
{
    @Override
    public boolean mustShow(AttributeBuilder attrBuilder)
    {
        String frontInputType = attrBuilder.getFrontendInput();
        String sourceModel    = attrBuilder.getSourceModel();
        
        if (StringUtils.isBlank(frontInputType)) {
            return false;
        }
        
        return (FrontendInputCombo.TYPE_SELECT.equals(frontInputType) || FrontendInputCombo.TYPE_MULTISELECT.equals(frontInputType)) &&
               StringUtils.isBlank(sourceModel);
    }

    @Override
    public String getText()
    {
        return "When using a select or a multiselect frontend input, provide or create a Source model for it. It keeps the Attribute's options separate from its HTML representation. "
               + "You can create one with the \"New Source Model\" Wizard or you can implement one your own. The Source model must extend <code>Mage_Eav_Model_Entity_Attribute_Source_Abstract</code>.";
    }

    @Override
    public String getTitle()
    {
        return "Source model is missing";
    }
}
