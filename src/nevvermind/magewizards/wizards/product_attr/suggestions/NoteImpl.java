/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions;

import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service=Service.class)
public class NoteImpl implements Service 
{
    private AttributeBuilder attrBuilder;
    
    @Override
    public boolean mustShow(AttributeBuilder attrBuilder)
    {
        this.attrBuilder = attrBuilder;
        return hasNoNote() || isSmallNote();
    }
    
    private boolean hasNoNote()
    {
        return StringUtils.isBlank(this.attrBuilder.getNote());
    }
    
    private boolean isSmallNote()
    {
        return !hasNoNote() && this.attrBuilder.getNote().length() <= 10;
    }

    @Override
    public String getText()
    {
        String commonSuggestion = " Describe the role of the attribute, its proper usage etc. in domain-specific, known or layman's terms. You can use HTML tags for highlighting.";
        
        if (isSmallNote()) {
            return "The Note you provided is too short." + commonSuggestion;
        }
        
        return "Please consider adding a proper Note to your attribute. Not only it will help the admin user, but maybe you too in a couple of months." +
               commonSuggestion;
    }
    
    @Override
    public String getTitle()
    {
        return isSmallNote() ? "Note too small" : "Note is missing";
    }    

}
