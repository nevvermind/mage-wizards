/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions.configurable;

import nevvermind.magewizards.ui.swing.TrueFalseCombo;
import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.GlobalCombo;
import nevvermind.magewizards.wizards.product_attr.suggestions.Service;
import org.openide.util.lookup.ServiceProvider;

/**
 * A Configurable-compatible attribute can be used in the creation form of the Configurable (super)
 * Product.
 * 
 * We can't know if the user plans to add a Source model. So discard that check, but tell him
 * that it's mandatory.
 * 
 * From <code>Mage_Catalog_Model_Product_Type_Configurable</code>:
 * <code>
 * public function canUseAttribute(Mage_Eav_Model_Entity_Attribute $attribute)
 * {
 *     $allow = $attribute->getIsGlobal() == Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL
 *         && $attribute->getIsVisible()
 *         && $attribute->getIsConfigurable()
 *         && $attribute->usesSource() // my note: this is actually only "select" with a source
 *         && $attribute->getIsUserDefined();
*
 *     return $allow;
 * }
 * </code>
 */
@ServiceProvider(service=Service.class)
public class ConfigurableCompatible implements Service 
{
    AttributeBuilder attrBuilder;
    
    boolean isGlobal;
    boolean isVisible;
    boolean isConfigurable;
    boolean isUserDefined;    
    
    @Override
    public boolean mustShow(AttributeBuilder attrBuilder)
    {
        this.attrBuilder = attrBuilder;
        
        isGlobal = attrBuilder.getGlobal().equals(GlobalCombo.SCOPE_GLOBAL);
        isVisible = attrBuilder.getVisible().equals(TrueFalseCombo.TRUE);
        isConfigurable = attrBuilder.getConfigurable().equals(TrueFalseCombo.TRUE);
        isUserDefined = attrBuilder.getUserDefined().equals(TrueFalseCombo.TRUE);
        
        return isConfigurable && 
               (!isGlobal || 
                !isVisible || 
                !isUserDefined || 
                !attrBuilder.getFrontendInput().equals(FrontendInputCombo.TYPE_SELECT) ||
                StringUtils.isBlank(attrBuilder.getSourceModel())
               );
    }

    @Override
    public String getText()
    {
        StringBuilder msg = new StringBuilder("You set the Attribute to be Configurable, but one (or all) of the following conditions were not met:");
        
        if (!isGlobal) {
            msg.append("<br /> - <code>scope = global</code>");
        }
        
        if (!isVisible) {
            msg.append("<br /> - <code>visible = true</code>");
        }
        
        if (!isUserDefined) {
            msg.append("<br /> - <code>user defined = true</code>");
        }
        
        if (!attrBuilder.getFrontendInput().equals(FrontendInputCombo.TYPE_SELECT)) {
            msg.append("<br /> - <code>frontend input = select</code>");
        }
        
        if (StringUtils.isBlank(attrBuilder.getSourceModel())) {
            msg.append("<br /> - needs a Source model");
        }
        
        return msg.toString();
    }

    @Override
    public String getTitle()
    {
        return "Your Attribute is incompatible with a Configurable Product";
    }
}
