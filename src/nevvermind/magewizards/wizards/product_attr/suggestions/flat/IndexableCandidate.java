/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions.flat;

import nevvermind.magewizards.ui.swing.TrueFalseCombo;
import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.BackendTypeCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import nevvermind.magewizards.wizards.product_attr.suggestions.Service;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service=Service.class)
public class IndexableCandidate implements Service 
{
    private AttributeBuilder attrBuilder;
    
    @Override
    public boolean mustShow(AttributeBuilder attrBuilder)
    {
        Service flatColCandidate = getFlatColumnCandidateImpl();
        
        if (flatColCandidate == null || !flatColCandidate.mustShow(attrBuilder)) {
            return false;
        }        
        
        this.attrBuilder = attrBuilder;
        boolean isTextBackendType = attrBuilder.getBackendType().equals(BackendTypeCombo.TYPE_TEXT);
        
        // a text type cannot be indexed (without performance penalty); so Mage's ditching it as a flat column index
        return !isTextBackendType && attrBuilder.getUsedForSortBy().equals(TrueFalseCombo.TRUE);
    }

    @Override
    public String getText()
    {
        if (attrBuilder.getFrontendInput().equals(FrontendInputCombo.TYPE_SELECT) ||
            attrBuilder.getFrontendInput().equals(FrontendInputCombo.TYPE_MULTISELECT)
        ) {
            return "In case you are (or will be) providing a Source model for this Attribute (which you should), that Source model must implement the <code>getFlatIndexes</code> method. " +
                   "You can create one with the \"New Source Model\" Wizard.";            
        }
        return "";
    }

    @Override
    public String getTitle()
    {
        return "Your attribute is \"indexable\" as a Flat Column";
    }
    
    protected Service getFlatColumnCandidateImpl()
    {
        for (Service impl : Lookup.getDefault().lookupAll(Service.class)) {
            if (impl instanceof ColumnCandidate) {
                return impl;
            }
        }
        return null;
    }    
}
