/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions.flat;

import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.BackendTypeCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import nevvermind.magewizards.wizards.product_attr.suggestions.Service;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 * If this is Flat Column candidate, check if the user should implement a Source model
 * (because he's using "select" or "multiselect").
 * If he does, suggest implementing the necessary methods in that Source.
 * 
 * In <code>Mage_Eav_Model_Entity_Attribute_Abstract</code>:
 * <code>
 * public function getFlatColumns()
 * {
 *     // If source model exists - get definition from it
 *     if ($this->usesSource() && $this->getBackendType() != self::TYPE_STATIC) {
 *         return $this->getSource()->getFlatColums();
 *     }
 *     // [...]
 * }
 * </code>
 */
@ServiceProvider(service=Service.class)
public class ColumnCandidateWithSource implements Service 
{
    @Override
    public boolean mustShow(AttributeBuilder attrBuilder)
    {
        Service flatColCandidate = getFlatColumnCandidateImpl();
        
        if (flatColCandidate == null) {
            return false;
        }
        
        String frontInputType = attrBuilder.getFrontendInput();
        
        return flatColCandidate.mustShow(attrBuilder) &&
               !attrBuilder.getBackendType().equals(BackendTypeCombo.TYPE_STATIC) && 
               (FrontendInputCombo.TYPE_SELECT.equals(frontInputType) || FrontendInputCombo.TYPE_MULTISELECT.equals(frontInputType));
    }
    
    @Override
    public String getText()
    {
        return "It's recommended that your (multi)select " + 
               "Source model implements the <code>getFlatColums</code> and <code>getFlatUpdateSelect</code> methods. You can use the \"New Source Model\" Wizard " +
               "to create those.";
    }

    @Override
    public String getTitle()
    {
        return "Use a \"flat-ready\" Source model";
    }
    
    protected Service getFlatColumnCandidateImpl()
    {
        for (Service impl : Lookup.getDefault().lookupAll(Service.class)) {
            if (impl instanceof ColumnCandidate) {
                return impl;
            }
        }
        return null;
    }
}
