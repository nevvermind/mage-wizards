/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions.flat;

import nevvermind.magewizards.ui.swing.TrueFalseCombo;
import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.BackendTypeCombo;
import nevvermind.magewizards.wizards.product_attr.suggestions.Service;
import org.openide.util.lookup.ServiceProvider;

/**
 * The attribute becomes a flat table column if:
 *     - is_used_for_promo_rules = 1 OR
 *     - used_in_product_listing = 1 OR
 *     - used_for_sort_by = 1 OR
 *     - backend_type = 'static'
 */
@ServiceProvider(service=Service.class)
public class ColumnCandidate implements Service {

    @Override
    public boolean mustShow(AttributeBuilder attrBuilder)
    {
        String usedForPromoRules    = attrBuilder.getUsedForPromoRules();
        String usedForSortBy        = attrBuilder.getUsedForSortBy();
        String usedInProductListing = attrBuilder.getUsedInProductListing();
        String backendType          = attrBuilder.getBackendType();
        
        boolean isUsedForPromoRules    = StringUtils.isNotBlank(usedForPromoRules)    && usedForPromoRules.equals(TrueFalseCombo.TRUE);
        boolean isUsedForSortBy        = StringUtils.isNotBlank(usedForSortBy)        && usedForSortBy.equals(TrueFalseCombo.TRUE);
        boolean isUsedInProductListing = StringUtils.isNotBlank(usedInProductListing) && usedInProductListing.equals(TrueFalseCombo.TRUE);
        boolean isStatic               = StringUtils.isNotBlank(backendType)          && attrBuilder.getBackendType().equals(BackendTypeCombo.TYPE_STATIC);
        
        return isUsedForPromoRules || isUsedForSortBy || isUsedInProductListing || isStatic;
    }

    @Override
    public String getText()
    {
        return "If you use the \"Flat Product Catalog\" feature, on reindexing, this Attribute will become a Flat Column in the <code>catalog_product_flat_*</code> table(s).";
    }

    @Override
    public String getTitle()
    {
        return "Your Attribute is a Flat Column candidate";
    }
}
