/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.suggestions;

import nevvermind.magewizards.ui.swing.TrueFalseCombo;
import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.wizards.product_attr.AttributeBuilder;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.BackendTypeCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.specific.IsFilterableCombo;
import org.openide.util.lookup.ServiceProvider;

/**
 * This is about an Attribute being <b>indexable by the Layered Navigation<b> logic (on Product Save).
 * 
 * Based on (from <code>Mage_Catalog_Model_Resource_Eav_Attribute</code>):
 * <code>
    public function isIndexable()
    {
        // exclude price attribute
        if ($this->getAttributeCode() == 'price') {
            return false;
        }

        if (!$this->getIsFilterableInSearch() && !$this->getIsVisibleInAdvancedSearch() && !$this->getIsFilterable()) {
            return false;
        }

        $backendType = $this->getBackendType();
        $frontendInput = $this->getFrontendInput();

        if ($backendType == 'int' && $frontendInput == 'select') {
            return true;
        } else if ($backendType == 'varchar' && $frontendInput == 'multiselect') {
            return true;
        } else if ($backendType == 'decimal') {
            return true;
        }

        return false;
    }
    * </code>
 */
@ServiceProvider(service=Service.class)
public class LayeredNavIndexableAttribute implements Service 
{
    @Override
    public boolean mustShow(AttributeBuilder attrBuilder)
    {
        boolean isFilterable = StringUtils.isNotBlank(attrBuilder.getFilterable()) &&
                               (attrBuilder.getFilterable().equals(IsFilterableCombo.VAL_YES_RESULTS) || 
                                attrBuilder.getFilterable().equals(IsFilterableCombo.VAL_YES_NO_RESULTS));
        
        boolean isVisibleInAdvancedSearch = attrBuilder.getVisibleInAdvancedSearch().equals(TrueFalseCombo.TRUE);
        boolean isFilterableInSearch = attrBuilder.getFilterableInSearch().equals(TrueFalseCombo.TRUE);
        
        if (!isFilterable || !isFilterableInSearch || !isVisibleInAdvancedSearch) {
            return false;
        }
        
        String backendType = attrBuilder.getBackendType();
        String frontendInput = attrBuilder.getFrontendInput();
        
        if (backendType.equals(BackendTypeCombo.TYPE_INT) && frontendInput.equals(FrontendInputCombo.TYPE_SELECT)) {
            return true;
        } else if (backendType.equals(BackendTypeCombo.TYPE_VARCHAR) && frontendInput.equals(FrontendInputCombo.TYPE_MULTISELECT)) {
            return true;
        } else if (backendType.equals(BackendTypeCombo.TYPE_DECIMAL)) {
            return true;
        }
        
        return false;
    }

    @Override
    public String getText()
    {
        return "Your Attribute will appear in \"anchor\" categories' Layered Navigation.";
    }

    @Override
    public String getTitle()
    {
        return "Your Attribute is Layered Nav \"indexable\"";
    }
}
