/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.preselect;

import java.util.List;
import nevvermind.magewizards.wizards.product_attr.UIVisualPanel1;
import nevvermind.magewizards.wizards.product_attr.UIWizardPanel1;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.BackendTypeCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.GlobalCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.SourceModelCombo;
import org.openide.WizardDescriptor;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service=ServiceAbstract.class)
public class GlobalYesNoImpl extends ServiceAbstract {

    @Override
    public void preselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        UIVisualPanel1 panel1 = ((UIWizardPanel1) panels.get(0)).getComponent();
        
        // "select" frontend input; this should be called first, because it changes the backend types available
        preselectComponent(panel1.frontendInputCheck, null);
        preselectComponent(panel1.frontendInputCombo, FrontendInputCombo.TYPE_SELECT);
        
        // backend type to "int"
        preselectComponent(panel1.backendTypeCheck, null);
        preselectComponent(panel1.backendTypeCombo, BackendTypeCombo.TYPE_INT);
        
        // is global
        preselectComponent(panel1.globalCheck, null);
        preselectComponent(panel1.globalCombo, GlobalCombo.SCOPE_GLOBAL);  
        
        // source model to "boolean"
        preselectComponent(panel1.sourceModelCheck, null);
        preselectComponent(panel1.sourceModelCombo, SourceModelCombo.TYPE_BOOLEAN);  
        
        // make the field editable if it's on default
        panel1.defaultValueCheck.setEnabled(true);
        if (panel1.defaultValueCheck.isSelected()) {
            panel1.defaultValueCheck.doClick(1);
        }
        // mark the default value to "No"
        panel1.defaultValueTextField.setText("0");
    }

    @Override
    public void deselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        UIVisualPanel1 panel1 = ((UIWizardPanel1) panels.get(0)).getComponent();
        
        deselectComponent(panel1.backendTypeCheck);
        deselectComponent(panel1.backendTypeCombo);
        deselectComponent(panel1.frontendInputCheck);
        deselectComponent(panel1.frontendInputCombo);
        deselectComponent(panel1.globalCheck);
        deselectComponent(panel1.globalCombo);
        deselectComponent(panel1.sourceModelCheck);
        deselectComponent(panel1.sourceModelCombo);
        deselectComponent(panel1.defaultValueCheck);
        deselectComponent(panel1.defaultValueTextField);
    }    

    @Override
    public String getLabel()
    {
        return "Global \"Yes/No\" Dropdown";
    }
}
