/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.preselect.layered_nav;

import java.util.List;
import javax.swing.DefaultComboBoxModel;
import nevvermind.magewizards.ui.swing.TrueFalseCombo;
import nevvermind.magewizards.wizards.product_attr.UIVisualPanel2;
import nevvermind.magewizards.wizards.product_attr.UIWizardPanel2;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.specific.IsFilterableCombo;
import nevvermind.magewizards.wizards.product_attr.preselect.ServiceAbstract;
import org.openide.WizardDescriptor;

public abstract class Abstract extends ServiceAbstract
{
    @Override
    public void preselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        UIVisualPanel2 panel2 = ((UIWizardPanel2) panels.get(1)).getComponent();        
        
        // visible in adv. search
        preselectComponent(panel2.isVisibleInAdvancedSearchCheck, null);
        preselectComponent(panel2.isVisibleInAdvancedSearchCombo, TrueFalseCombo.TRUE);        
        
        // is filterable in search
        preselectComponent(panel2.isFilterableInSearchCheck, null);
        preselectComponent(panel2.isFilterableInSearchCombo, TrueFalseCombo.TRUE);   
        
        String[] newValues = new String[] { IsFilterableCombo.VAL_YES_RESULTS, IsFilterableCombo.VAL_YES_NO_RESULTS };
        panel2.isFilterableCombo.setModel(new DefaultComboBoxModel<String>(newValues));
        panel2.isFilterableCombo.setSelectedItem(IsFilterableCombo.VAL_YES_RESULTS);
    }
    
    @Override
    public void deselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        UIVisualPanel2 panel2 = ((UIWizardPanel2) panels.get(1)).getComponent();        
        
        deselectComponent(panel2.isVisibleInAdvancedSearchCheck);
        deselectComponent(panel2.isVisibleInAdvancedSearchCombo);
        deselectComponent(panel2.isFilterableInSearchCheck);
        deselectComponent(panel2.isFilterableInSearchCombo);
        deselectComponent(panel2.isFilterableCheck);
        
        panel2.isFilterableCombo.setModel(new DefaultComboBoxModel<String>(IsFilterableCombo.VALUES));
        deselectComponent(panel2.isFilterableCombo);
        
    }
}
