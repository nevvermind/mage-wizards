/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.preselect;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import nevvermind.magewizards.wizards.product_attr.UIVisualPanel2;
import nevvermind.magewizards.wizards.product_attr.UIWizardPanel2;
import nevvermind.magewizards.wizards.product_attr.UIVisualPanel1;
import nevvermind.magewizards.wizards.product_attr.UIWizardPanel1;
import org.openide.WizardDescriptor;

/**
 * This exists by default and does not count as a service provider.
 */
public class DefaultImpl extends ServiceAbstract 
{
    final private Set<javax.swing.JCheckBox> allChecks = new HashSet<javax.swing.JCheckBox>();
    
    @Override
    public void preselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        for (javax.swing.JCheckBox check : getDefaultableCheckboxes(panels)) {
            
            // some events might disable the check
            if (!check.isEnabled()) {
                check.setEnabled(true);
            }
            
            if (!check.isSelected()) {
                check.doClick(1); // do it quick
            }
        }
    }
    
    protected Set<javax.swing.JCheckBox> getDefaultableCheckboxes(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        if (allChecks.isEmpty()) {
            UIVisualPanel1 panel1 = ((UIWizardPanel1) panels.get(0)).getComponent();
            UIVisualPanel2 panel2 = ((UIWizardPanel2) panels.get(1)).getComponent();
            
            allChecks.add(panel1.backendTypeCheck);
            allChecks.add(panel1.backendTableCheck);
            allChecks.add(panel1.frontendModelCheck);
            allChecks.add(panel1.frontendInputCheck);
            allChecks.add(panel1.frontendClassCheck);
            allChecks.add(panel1.groupCheck);
            allChecks.add(panel1.isRequiredCheck);
            allChecks.add(panel1.isUserDefinedCheck);
            allChecks.add(panel1.defaultValueCheck);
            allChecks.add(panel1.isUniqueCheck);
            allChecks.add(panel1.globalCheck);
            allChecks.add(panel1.sourceModelCheck);
            allChecks.add(panel1.backendModelCheck);

            allChecks.add(panel2.isVisibleCheck);
            allChecks.add(panel2.isSearchableCheck);
            allChecks.add(panel2.isComparableCheck);
            allChecks.add(panel2.isVisibleOnFrontCheck);
            allChecks.add(panel2.isWysiwygEnabledCheck);
            allChecks.add(panel2.isHtmlAllowedOnFrontCheck);
            allChecks.add(panel2.isVisibleInAdvancedSearchCheck);
            allChecks.add(panel2.isFilterableInSearchCheck);
            allChecks.add(panel2.usedInProductListingCheck);
            allChecks.add(panel2.usedForSortByCheck);
            allChecks.add(panel2.isConfigurableCheck);
            allChecks.add(panel2.isUsedForPromoRulesCheck);
            allChecks.add(panel2.frontendInputRendererCheck);
            allChecks.add(panel2.isFilterableCheck);
            allChecks.add(panel2.positionTextFieldCheck);
            allChecks.add(panel2.applyToListCheck);
        }
        
        return allChecks;
    }

    @Override
    public String getLabel()
    {
        return "Use Defaults";
    }

    @Override
    public void deselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        
    }
}
