/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.preselect;

import java.util.List;
import nevvermind.magewizards.ui.swing.TrueFalseCombo;
import nevvermind.magewizards.wizards.product_attr.UIVisualPanel1;
import nevvermind.magewizards.wizards.product_attr.UIVisualPanel2;
import nevvermind.magewizards.wizards.product_attr.UIWizardPanel1;
import nevvermind.magewizards.wizards.product_attr.UIWizardPanel2;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.GlobalCombo;
import org.openide.WizardDescriptor;
import org.openide.util.lookup.ServiceProvider;

/**
 * We do everything except setting a Source model. The Suggestions will pick that up.
 */
@ServiceProvider(service=ServiceAbstract.class)
public class Configurable extends ServiceAbstract 
{
    @Override
    public void preselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        UIVisualPanel1 panel1 = ((UIWizardPanel1) panels.get(0)).getComponent();
        UIVisualPanel2 panel2 = ((UIWizardPanel2) panels.get(1)).getComponent();
        
        // "select" frontend input
        preselectComponent(panel1.frontendInputCheck, null);
        preselectComponent(panel1.frontendInputCombo, FrontendInputCombo.TYPE_SELECT);
        
        // is global
        preselectComponent(panel1.globalCheck, null);
        preselectComponent(panel1.globalCombo, GlobalCombo.SCOPE_GLOBAL);
        
        // user defined
        preselectComponent(panel1.isUserDefinedCheck, null);
        preselectComponent(panel1.isUserDefinedCombo, TrueFalseCombo.TRUE);
        
        // is visible
        preselectComponent(panel2.isVisibleCheck, null);
        preselectComponent(panel2.isVisibleCombo, TrueFalseCombo.TRUE);
        
        // is configurable
        preselectComponent(panel2.isConfigurableCheck, null);
        preselectComponent(panel2.isConfigurableCombo, TrueFalseCombo.TRUE);
    }

    @Override
    public void deselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        UIVisualPanel1 panel1 = ((UIWizardPanel1) panels.get(0)).getComponent();
        UIVisualPanel2 panel2 = ((UIWizardPanel2) panels.get(1)).getComponent();
        
        deselectComponent(panel1.frontendInputCheck);
        deselectComponent(panel1.frontendInputCombo);
        deselectComponent(panel1.globalCheck);
        deselectComponent(panel1.globalCombo);
        deselectComponent(panel1.isUserDefinedCheck);
        deselectComponent(panel1.isUserDefinedCombo);
        deselectComponent(panel2.isVisibleCheck);
        deselectComponent(panel2.isVisibleCombo);
        deselectComponent(panel2.isConfigurableCheck);
        deselectComponent(panel2.isConfigurableCombo);
    }
    
    @Override
    public String getLabel()
    {
        return "Configurable";
    }
}
