/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.preselect;

import java.util.List;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.IDefaultable;
import org.openide.WizardDescriptor;

public abstract class ServiceAbstract
{
    public abstract void preselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels);
    
    public abstract void deselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels);
    
    public abstract String getLabel();

    @Override
    public String toString()
    {
        return getLabel();
    }

    /**
     * Common pre-selection logic for all Components.
     * 
     * @param component 
     */
    private void preselectComponent(JComponent component)
    {
        component.setEnabled(false);
    }
    
    /**
     * Triggers common pre-selection logic, then applies specific ones.
     * 
     * @param component
     * @param preSelectedValue 
     */
    public void preselectComponent(JComponent component, Object preSelectedValue)
    {
        preselectComponent(component);
        
        if (component instanceof JCheckBox) {
            ((JCheckBox) component).setSelected(false);
        }        
        
        if (component instanceof JComboBox) {
            ((JComboBox) component).setSelectedItem(preSelectedValue);
        }
    }
    
    /**
     * @param component 
     */
    public void deselectComponent(JComponent component)
    {
        component.setEnabled(true);
        
        // set defaults if they have one
        if (component instanceof IDefaultable) {
            ((IDefaultable) component).setDefault();
        }
        
        // checkboxes need to be checked to simulate defaults
        if (component instanceof JCheckBox) {
            ((JCheckBox) component).setSelected(true);
        }
        
        // combos need to be disabled to simulate default
        if (component instanceof JComboBox) {
            component.setEnabled(false);
        }
        
        if (component instanceof JTextField) {
            component.setEnabled(false);
        }
    }
}
