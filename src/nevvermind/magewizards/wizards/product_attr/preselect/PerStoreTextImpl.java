/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.product_attr.preselect;

import java.util.List;
import nevvermind.magewizards.wizards.product_attr.UIVisualPanel1;
import nevvermind.magewizards.wizards.product_attr.UIWizardPanel1;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.BackendTypeCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.FrontendInputCombo;
import nevvermind.magewizards.wizards.product_attr.attribute.attribute.base.GlobalCombo;
import org.openide.WizardDescriptor;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service=ServiceAbstract.class)
public class PerStoreTextImpl extends ServiceAbstract 
{
    @Override
    public void preselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        UIVisualPanel1 component = ((UIWizardPanel1) panels.get(0)).getComponent();
        
        preselectComponent(component.backendTypeCheck, null);
        preselectComponent(component.backendTypeCombo, BackendTypeCombo.TYPE_VARCHAR);
        
        preselectComponent(component.frontendInputCheck, null);
        preselectComponent(component.frontendInputCombo, FrontendInputCombo.TYPE_TEXT);
        
        preselectComponent(component.globalCheck, null);
        preselectComponent(component.globalCombo, GlobalCombo.SCOPE_STORE);
    }

    @Override
    public void deselect(List<WizardDescriptor.Panel<WizardDescriptor>> panels)
    {
        UIVisualPanel1 component = ((UIWizardPanel1) panels.get(0)).getComponent();
        
        deselectComponent(component.backendTypeCheck);
        deselectComponent(component.backendTypeCombo);
        deselectComponent(component.frontendInputCheck);
        deselectComponent(component.frontendInputCombo);
        deselectComponent(component.globalCheck);
        deselectComponent(component.globalCombo);
    }    

    @Override
    public String getLabel()
    {
        return "Per-Store Text";
    }
}
