/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.source;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import javax.script.ScriptException;
import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.utils.files.Freemarker;
import nevvermind.magewizards.wizards.source.ui.OldColumnTypesCombo;
import nevvermind.magewizards.wizards.source.ui.SourceTypeCombo;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public enum FileCreator
{
    INSTANCE;
    
    private static final String TPL_PATH = "org-netbeans-modules-php-phpproject/Mage Wizards/source/Source.php";
    private static final String IN_MODULE_PATH_CONFIG = "/Model/System/Config/Source/";
    private static final String IN_MODULE_PATH_ATTR = "/Model/Attribute/Source/";
    private static final String INTERIM_CLASS_CONFIG = "Model_System_Config_Source";
    private static final String INTERIM_CLASS_ATTR = "Model_Attribute_Source";
    
    private Project project;
    private String moduleNs;
    private String moduleName;
    private String helperAlias;
    private Collection<String[]> options = new ArrayList<String[]>();
    private boolean mustCreateIndex = false;
    private boolean mustCreateFlatColumnsDefinition = true;
    private String sourceType = SourceTypeCombo.TYPE_BOTH;
    private String sourceFileName;
    private boolean colNullable;
    private boolean colUnsigned;
    private boolean colHasDefaultValue;
    private String colDefaultValue;
    private String oldStyleColumnType;
    private String columnLength;
    
    public void reset()
    {
        project = null;
        moduleNs = null;
        moduleName = null;
        helperAlias = null;
        options = new ArrayList<String[]>();
        mustCreateIndex = false;
        mustCreateFlatColumnsDefinition = true;
        sourceType = SourceTypeCombo.TYPE_BOTH;
        sourceFileName = null;
        colNullable = true;
        colUnsigned = true;
        colHasDefaultValue = false;
        colDefaultValue = null;
        oldStyleColumnType = null;
        columnLength = null;
    }
    
    public Project getProject()
    {
        if (project == null) {
            project = Utilities.actionsGlobalContext().lookup(Project.class);
        }
        return project;
    }
    
    protected String getProjectRootPath()
    {
        return FileUtil.toFile(getProject().getProjectDirectory()).getPath();
    }
    
    protected String getModulePath()
    {
        return "/app/code/local/" + StringUtils.asTitle(getModuleNs()) + "/" + StringUtils.asTitle(getModuleName());
    }
    
    public final String getFullRelativePath()
    {
        String inModulePath;
        if (SourceTypeCombo.TYPE_CONFIG.equals(getSourceType())) {
            inModulePath = IN_MODULE_PATH_CONFIG;
        } else {
            inModulePath = IN_MODULE_PATH_ATTR;
        }
        
        return getModulePath() + inModulePath + StringUtils.asTitle(getSourceFileName()) + ".php";
    }
    
    public final String getFullFilePath()
    {
        return getProjectRootPath() + getFullRelativePath();
    }

    public boolean mustCreateFlatColumnsDefinition()
    {
        return mustCreateFlatColumnsDefinition;
    }

    public void setMustCreateFlatColumnsDefinition(boolean flag)
    {
        this.mustCreateFlatColumnsDefinition = flag;
    }

    public String getModuleNs()
    {
        return moduleNs;
    }

    public void setModuleNs(String moduleNs)
    {
        this.moduleNs = moduleNs;
    }

    public String getModuleName()
    {
        return moduleName;
    }

    public void setModuleName(String moduleName)
    {
        this.moduleName = moduleName;
    }

    public String getHelperAlias()
    {
        return helperAlias;
    }

    public void setHelperAlias(String helperAlias)
    {
        this.helperAlias = helperAlias;
    }

    public Collection<String[]> getOptions()
    {
        return options;
    }

    public void setOptions(Collection<String[]> options)
    {
        this.options = options;
    }

    public boolean mustCreateIndex()
    {
        return mustCreateFlatColumnsDefinition() && mustCreateIndex;
    }

    public void setMustCreateIndex(boolean mustCreateIndex)
    {
        this.mustCreateIndex = mustCreateIndex;
    }

    public String getSourceType()
    {
        return sourceType;
    }

    public void setSourceType(String sourceType)
    {
        this.sourceType = sourceType;
    }
    
    protected boolean isAttributeSource()
    {
        return SourceTypeCombo.TYPE_ATTRIBUTE.equals(getSourceType()) ||
               SourceTypeCombo.TYPE_BOTH.equals(getSourceType());
    }
    
    protected boolean isConfigSource()
    {
        return SourceTypeCombo.TYPE_CONFIG.equals(getSourceType()) ||
               SourceTypeCombo.TYPE_BOTH.equals(getSourceType());
    }

    public String getSourceFileName()
    {
        return sourceFileName;
    }

    public void setSourceFileName(String sourceFileName)
    {
        this.sourceFileName = sourceFileName;
    }

    public boolean isColNullable()
    {
        return colNullable;
    }

    public void setColNullable(boolean colNullable)
    {
        this.colNullable = colNullable;
    }

    public boolean isColUnsigned()
    {
        return colUnsigned;
    }

    public void setColUnsigned(boolean colUnsigned)
    {
        this.colUnsigned = colUnsigned;
    }

    public boolean isColHasDefaultValue()
    {
        return colHasDefaultValue;
    }

    public void setColHasDefaultValue(boolean colHasDefaultValue)
    {
        this.colHasDefaultValue = colHasDefaultValue;
    }

    public String getColDefaultValue()
    {
        return colDefaultValue;
    }

    public void setColDefaultValue(String colDefaultValue)
    {
        this.colDefaultValue = colDefaultValue;
    }

    public String getOldStyleColumnType()
    {
        return oldStyleColumnType;
    }

    public void setOldStyleColumnType(String oldStyleColumnType)
    {
        this.oldStyleColumnType = oldStyleColumnType;
    }

    public String getColumnLength()
    {
        return columnLength;
    }

    public void setColumnLength(String columnLength)
    {
        this.columnLength = columnLength;
    }
    
    protected String getClassName()
    {
        String classPrefix;
        if (SourceTypeCombo.TYPE_CONFIG.equals(getSourceType())) {
            classPrefix = INTERIM_CLASS_CONFIG;
        } else {
            classPrefix = INTERIM_CLASS_ATTR;
        }
        
        return StringUtils.asTitle(getModuleNs()) + 
               "_" + 
               StringUtils.asTitle(getModuleName()) + 
               "_" + 
               classPrefix + 
               "_" + 
               StringUtils.asTitle(getSourceFileName());
    }
    
    public final void createSourceFile()
    {
        Map<String, Object> tplData = new HashMap<String, Object>();
        tplData.put("helperAlias", getHelperAlias());
        tplData.put("mustCreateFlatColsDefs", mustCreateFlatColumnsDefinition());
        tplData.put("mustCreateIndex", mustCreateIndex());
        tplData.put("isAttributeSource", isAttributeSource());
        tplData.put("isConfigSource", isConfigSource());
        tplData.put("className", getClassName());
        
        Collection<HashMap<String, String>> tplOptions = new ArrayList<HashMap<String, String>>();
        for (String[] userOption : getOptions()) {
            HashMap<String, String> option = new HashMap<String, String>();
            option.put("value", userOption[0]);
            option.put("label", userOption[1]);
            tplOptions.add(option);
        }
        tplData.put("options", tplOptions);
        
        Map<String, String> columnDefinition = new HashMap<String, String>();
        columnDefinition.put("nullable", isColNullable() ? "true" : "false");
        columnDefinition.put("unsigned", isColUnsigned() ? "true" : "false");
        columnDefinition.put("defaultValue", getColDefaultValue() == null ? "null" : "'" + getColDefaultValue() + "'");
        
        if (getColumnLength() != null) {
            columnDefinition.put("length", getColumnLength());
        }
        
        columnDefinition.put("oldType", getOldStyleColumnType());
        columnDefinition.put("newType", OldColumnTypesCombo.getOld2NewColsMap().get(getOldStyleColumnType()));
        
        tplData.put("flatCol", columnDefinition);
        
        try {        
            Freemarker.createFileFromTemplate(FileUtil.getConfigFile(TPL_PATH), new File(getFullFilePath()), tplData);
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (ScriptException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
