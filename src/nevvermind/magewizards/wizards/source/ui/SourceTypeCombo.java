/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.source.ui;

import java.util.HashMap;
import java.util.Map;
import javax.swing.JComboBox;

public class SourceTypeCombo extends JComboBox<String>
{
    public static final String TYPE_ATTRIBUTE = "Attribute";
    public static final String TYPE_CONFIG    = "Config";
    public static final String TYPE_BOTH      = "Attribute + Config";
    
    private static final Map<String, String> descriptionMap = new HashMap<String, String>();
    static {
        descriptionMap.put(TYPE_ATTRIBUTE, "Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute Attribute ");
        descriptionMap.put(TYPE_CONFIG, "Config");
        descriptionMap.put(TYPE_BOTH, "Both");
    }
    
    public SourceTypeCombo()
    {
        super(new String[] {TYPE_ATTRIBUTE, TYPE_CONFIG, TYPE_BOTH});
    }

    public static Map<String, String> getDescriptionMap()
    {
        return descriptionMap;
    }
}
