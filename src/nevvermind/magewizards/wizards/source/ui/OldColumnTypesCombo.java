/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.source.ui;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class OldColumnTypesCombo extends javax.swing.JComboBox<String>
{
    public static final String COL_BOOL       = "bool";
    public static final String COL_CHAR       = "char";
    public static final String COL_VARCHAR    = "varchar";
    public static final String COL_TINYTEXT   = "tinytext";
    public static final String COL_TEXT       = "text";
    public static final String COL_MEDIUMTEXT = "mediumtext";
    public static final String COL_LONGTEXT   = "longtext";
    public static final String COL_BLOB       = "blob";
    public static final String COL_MEDIUMBLOB = "mediumblob";
    public static final String COL_LONGBLOB   = "longblob";
    public static final String COL_TINYINT    = "tinyint";
    public static final String COL_SMALLINT   = "smallint";
    public static final String COL_MEDIUMINT  = "mediumint";
    public static final String COL_INT        = "int";
    public static final String COL_BIGINT     = "bigint";
    public static final String COL_FLOAT      = "float";
    public static final String COL_DECIMAL    = "decimal";
    public static final String COL_NUMERIC    = "numeric";
    public static final String COL_DATETIME   = "datetime";
    public static final String COL_TIMESTAMP  = "timestamp";
    public static final String COL_TIME       = "time";
    public static final String COL_DATE       = "date";
    
    public static final String VARIEN_BOOLEAN   = "Varien_Db_Ddl_Table::TYPE_BOOLEAN";
    public static final String VARIEN_TEXT      = "Varien_Db_Ddl_Table::TYPE_TEXT";
    public static final String VARIEN_BLOB      = "Varien_Db_Ddl_Table::TYPE_BLOB";
    public static final String VARIEN_SMALLINT  = "Varien_Db_Ddl_Table::TYPE_SMALLINT";
    public static final String VARIEN_INTEGER   = "Varien_Db_Ddl_Table::TYPE_INTEGER";
    public static final String VARIEN_BIGINT    = "Varien_Db_Ddl_Table::TYPE_BIGINT";
    public static final String VARIEN_FLOAT     = "Varien_Db_Ddl_Table::TYPE_FLOAT";
    public static final String VARIEN_DECIMAL   = "Varien_Db_Ddl_Table::TYPE_DECIMAL";
    public static final String VARIEN_DATETIME  = "Varien_Db_Ddl_Table::TYPE_DATETIME";
    public static final String VARIEN_TIMESTAMP = "Varien_Db_Ddl_Table::TYPE_TIMESTAMP";
    public static final String VARIEN_DATE      = "Varien_Db_Ddl_Table::TYPE_DATE";
    
    public static final Map<String, String> old2NewColsMap = new HashMap<String, String>();
    public static final Map<String, String> colLengthMap   = new HashMap<String, String>();
    static {
        // ------------ old to new map
        
        old2NewColsMap.put(COL_BOOL, VARIEN_BOOLEAN);
        
        old2NewColsMap.put(COL_CHAR, VARIEN_TEXT);
        old2NewColsMap.put(COL_VARCHAR, VARIEN_TEXT);
        old2NewColsMap.put(COL_TINYTEXT, VARIEN_TEXT);
        old2NewColsMap.put(COL_TEXT, VARIEN_TEXT);
        old2NewColsMap.put(COL_MEDIUMTEXT, VARIEN_TEXT);
        old2NewColsMap.put(COL_LONGTEXT, VARIEN_TEXT);
        
        old2NewColsMap.put(COL_BLOB, VARIEN_BLOB);
        old2NewColsMap.put(COL_MEDIUMBLOB, VARIEN_BLOB);
        old2NewColsMap.put(COL_LONGBLOB, VARIEN_BLOB);
        
        old2NewColsMap.put(COL_TINYINT, VARIEN_SMALLINT);
        old2NewColsMap.put(COL_SMALLINT, VARIEN_SMALLINT);
        
        old2NewColsMap.put(COL_MEDIUMINT, VARIEN_INTEGER);
        old2NewColsMap.put(COL_INT, VARIEN_INTEGER);
        
        old2NewColsMap.put(COL_BIGINT, VARIEN_BIGINT);
        
        old2NewColsMap.put(COL_FLOAT, VARIEN_FLOAT);
        
        old2NewColsMap.put(COL_DECIMAL, VARIEN_DECIMAL);
        old2NewColsMap.put(COL_NUMERIC, VARIEN_DECIMAL);
        
        old2NewColsMap.put(COL_DATETIME, VARIEN_DATETIME);
        
        old2NewColsMap.put(COL_TIMESTAMP, VARIEN_TIMESTAMP);
        old2NewColsMap.put(COL_TIME, VARIEN_TIMESTAMP);
        
        old2NewColsMap.put(COL_DATE, VARIEN_DATE);
        
        // ------------ lengths map
        colLengthMap.put(COL_CHAR, "255");
        colLengthMap.put(COL_VARCHAR, "255");
        colLengthMap.put(COL_TINYTEXT, "255");
        colLengthMap.put(COL_TEXT, "64k");
        colLengthMap.put(COL_MEDIUMTEXT, "16M");
        colLengthMap.put(COL_LONGTEXT, "4g");
        colLengthMap.put(COL_BLOB, "64k");
        colLengthMap.put(COL_MEDIUMBLOB, "64k");
        colLengthMap.put(COL_LONGBLOB, "64k");
        colLengthMap.put(COL_DECIMAL, "12,4");
        colLengthMap.put(COL_NUMERIC, "12,4");
    }

    public OldColumnTypesCombo()
    {
        super(getDropdownValues());
    }
    
    private static String[] getDropdownValues()
    {
        String[] values = old2NewColsMap.keySet().toArray(new String[0]);
        Arrays.sort(values);
        return values;
    }
    
    public static Map<String, String> getOld2NewColsMap()
    {
        return old2NewColsMap;
    }
    
    public static Map<String, String> getColLengthMap()
    {
        return colLengthMap;
    }
}
