/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.source.ui;

import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListModel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;

public class OptionsList extends javax.swing.JList<String>
{
    private final static Map<Integer, String[]> options = new HashMap<Integer, String[]>();
    
    public OptionsList()
    {
        super();
        DefaultListModel<String> listModel = new DefaultListModel<String>();
        this.setModel(listModel);
        this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void addOption(final String value, final String label)
    {
        final DefaultListModel<String> model = (DefaultListModel<String>) this.getModel();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                model.<String>addElement(value + " -> " + label);
                options.put(model.getSize() - 1, new String[] {value, label});
            }
        });        
    }
    
    /**
     * Key as the JList index and String[] with [0] = value and [1] = label
     */
    public static Map<Integer, String[]> getOptions()
    {
        return options;
    }
    
    public void removeOption(final int index)
    {
        final DefaultListModel<String> model = (DefaultListModel<String>) this.getModel();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                model.remove(index);
                options.remove(index);
            }
        });
    }
}
