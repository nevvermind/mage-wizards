/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.source.nb_wizards;

import nevvermind.magewizards.wizards.source.FileCreator;
import nevvermind.magewizards.wizards.source.ui.OptionsList;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;

public class UIWizardPanel2 extends UIWizardPanelAbstract
{
    @Override
    protected UIVisualPanel2 getVisualPanel()
    {
        return new UIVisualPanel2();
    }

    @Override
    public void storeSettings(WizardDescriptor wiz)
    {
        super.storeSettings(wiz);
        
        FileCreator.INSTANCE.setOptions(OptionsList.getOptions().values());
    }
    

    @Override
    public void validate() throws WizardValidationException
    {
        super.validate();
        
        UIVisualPanel2 panel = (UIVisualPanel2) getComponent();
        
        if (panel.optionsList.getModel().getSize() == 0) {
            throw new WizardValidationException(null, "Add some options first.", null);
        }
    }
}
