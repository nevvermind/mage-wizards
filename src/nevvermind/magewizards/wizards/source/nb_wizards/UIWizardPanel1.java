/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.source.nb_wizards;

import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.wizards.source.FileCreator;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;

public class UIWizardPanel1 extends UIWizardPanelAbstract
{
    @Override
    protected UIVisualPanel1 getVisualPanel()
    {
        return new UIVisualPanel1();
    }
    
    @Override
    public void storeSettings(WizardDescriptor wiz)
    {
        super.storeSettings(wiz);
        
        UIVisualPanel1 panel = (UIVisualPanel1) getComponent();
        
        FileCreator fileCreator = FileCreator.INSTANCE;
        
        fileCreator.setMustCreateFlatColumnsDefinition(panel.createFlatColCheckbox.isEnabled() && panel.createFlatColCheckbox.isSelected());
        fileCreator.setHelperAlias(panel.helperAliasTextField.getText().trim());
        fileCreator.setModuleName(panel.moduleNameTextField.getText().trim());
        fileCreator.setModuleNs(panel.moduleNsTextField.getText().trim());
        fileCreator.setSourceType((String) panel.sourceTypeCombo.getSelectedItem());
        fileCreator.setSourceFileName(panel.fileNameTextField.getText().trim());
    }    

    @Override
    public void validate() throws WizardValidationException
    {
        super.validate();
        
        UIVisualPanel1 panel = (UIVisualPanel1) getComponent();
        
        // --------- Filename validations 
        String fileName = panel.fileNameTextField.getText().trim();
        
        if (!StringUtils.isNotBlank(fileName)) {
            throw new WizardValidationException(null, "No file name provided.", null);
        }
        
        if (!fileName.matches("\\w+")) {
            throw new WizardValidationException(null, "Invalid file name provided. Only a-z, _ or 0-9.", null);
        }
        
        // --------- Module NS validations 
        if (!StringUtils.isNotBlank(panel.moduleNsTextField.getText().trim())) {
            throw new WizardValidationException(null, "Specify the Module's Namespace.", null);
        }
        
        // --------- Module name validations 
        if (!StringUtils.isNotBlank(panel.moduleNameTextField.getText().trim())) {
            throw new WizardValidationException(null, "Specify the Module's Name.", null);
        }
        
        // --------- Helper alias validations 
        String helperAlias = panel.helperAliasTextField.getText().trim();
        if (helperAlias.endsWith("/data")) {
            helperAlias = helperAlias.substring(0, helperAlias.lastIndexOf("/data")).trim();
        }
        
        if (!StringUtils.isNotBlank(helperAlias)) {
            throw new WizardValidationException(null, "Specify a Helper alias.", null);
        }
        
        if (!helperAlias.matches("[a-zA-Z0-9](\\w+/*\\w+)*")) {
            throw new WizardValidationException(null, "Invalid Helper alias provided. Only a-z, A-Z, 0-9, _ or / are permitted and it must not end or begin with a forward slash.", null);
        }
        
        panel.fileNameTextField.setText(fileName);
        panel.helperAliasTextField.setText(helperAlias);
        panel.moduleNsTextField.setText(panel.moduleNsTextField.getText().trim());
        panel.moduleNameTextField.setText(panel.moduleNameTextField.getText().trim());
        
        setIsValid(true);
    }
}
