<?php

class ${className}
<#if isAttributeSource>
    extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
</#if>
{
    /**
     * @return Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        return Mage::helper('${helperAlias}');
    }
<#if isAttributeSource>

    /**
     * @param bool $withEmpty Add empty option to array
     * @return array
     */
    public function getAllOptions($withEmpty = true)
    {
        if (is_null($this->_options)) {
            $helper = $this->_getHelper();
            $this->_options = array(
                <#list options as option>
                array('value' => '${option.value}', 'label' => $helper->__('${option.label}')),
                </#list>
            );
        }
        return $this->_options;
    }
</#if>
<#if isConfigSource>

    /**
     * @return array
     */
    public function toOptionArray($isMultiselect = false)
    {
        <#if isAttributeSource>
        return $this->getAllOptions(!$isMultiselect);
        <#else>
        $helper = $this->_getHelper();
        return array(
            <#list options as option>
            array('value' => '${option.value}', 'label' => $helper->__('${option.label}')),
            </#list>
        );
        </#if>
    }
</#if>
<#if mustCreateFlatColsDefs>

    // <editor-fold defaultstate="collapsed" desc="Flat Column Definition">      
    /**
     * Retrieve flat column definition
     *
     * @return array
     */
    public function getFlatColums()
    {
        $attributeCode = $this->getAttribute()->getAttributeCode();
        $column = array(
            'unsigned' => ${flatCol.unsigned},
            'default'  => ${flatCol.defaultValue},
            'extra'    => null
        );

        if (Mage::helper('core')->useDbCompatibleMode()) {
            $column['type']    = '${flatCol.oldType}<#if flatCol.length??>(${flatCol.length})</#if>';
            $column['is_null'] = ${flatCol.nullable};
        } else {
            $column['type']     = ${flatCol.newType};
            $column['nullable'] = ${flatCol.nullable};
            $column['comment']  = $attributeCode . ' column';
        }

        return array($attributeCode => $column);
    }
<#if mustCreateIndex>

    /**
     * Retrieve Indexes for Flat
     *
     * @return array
     */
    public function getFlatIndexes()
    {
        $indexes = array();

        $index = sprintf('IDX_%s', strtoupper($this->getAttribute()->getAttributeCode()));
        $indexes[$index] = array(
            'type'      => 'index',
            'fields'    => array($this->getAttribute()->getAttributeCode())
        );

        return $indexes;
    }
</#if>

    /**
     * Retrieve Select For Flat Attribute update
     *
     * @param int $store
     * @return Varien_Db_Select
     */
    public function getFlatUpdateSelect($store)
    {
        return Mage::getResourceSingleton('eav/entity_attribute')
                   ->getFlatUpdateSelect($this->getAttribute(), $store);
    } // </editor-fold>
</#if>
}
