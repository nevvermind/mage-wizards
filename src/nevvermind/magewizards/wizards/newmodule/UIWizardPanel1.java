/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.newmodule;

import javax.swing.JComboBox;
import javax.swing.event.ChangeListener;
import org.openide.WizardDescriptor;
import org.openide.WizardValidationException;
import org.openide.util.HelpCtx;

public class UIWizardPanel1 implements WizardDescriptor.ValidatingPanel<WizardDescriptor>
{
    public final static String PROP_CREATOR = "creator";
    
    private boolean isValid = true;    
    
    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private UIVisualPanel1 component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public UIVisualPanel1 getComponent()
    {
        if (component == null) {
            component = new UIVisualPanel1();
        }
        return component;
    }

    @Override
    public HelpCtx getHelp()
    {
         return null;
    }

    @Override
    public boolean isValid()
    {
        // If it is always OK to press Next or Finish, then:
        return isValid;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
    }

    @Override
    public void addChangeListener(ChangeListener l)
    {
    }

    @Override
    public void removeChangeListener(ChangeListener l)
    {
    }

    @Override
    public void readSettings(WizardDescriptor wiz)
    {
        // use wiz.getProperty to retrieve previous panel state
    }

    @Override
    public void storeSettings(WizardDescriptor wiz)
    {
        UIVisualPanel1 panel = getComponent();
        
            FilesCreator creator = new FilesCreator();
            
            creator.setName(panel.getModuleName().getText());
            creator.setNamespace(panel.getModuleNs().getText());
            creator.setIsActive(panel.getModuleIsEnabled().getSelectedItem().toString());
            
            if (panel.getCreateInstallerCheck().isSelected()) {
                
                JComboBox<String> installerType = panel.getInstallerTypeCombo();
                creator.setMakeDataInstall(installerType.getSelectedIndex() == 1 || installerType.getSelectedIndex() == 2);
                creator.setMakeSqlInstall(installerType.getSelectedIndex() == 0  || installerType.getSelectedIndex() == 2);
                creator.setBaseInstallerClass((String) panel.getInstallerClassCombo().getSelectedItem());
            }
            
            wiz.putProperty(PROP_CREATOR, creator);
    }

    @Override
    public void validate() throws WizardValidationException
    {
        UIVisualPanel1 panel = getComponent();
        
        if (panel.getModuleNs().getText().equals("")) {
            throw new WizardValidationException(null, "Invalid module namespace", null);
        }
        
        if (panel.getModuleName().getText().equals("")) {
            throw new WizardValidationException(null, "Invalid module name", null);
        }
        
        isValid = true;
    }
}
