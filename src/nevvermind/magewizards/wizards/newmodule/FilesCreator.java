/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.wizards.newmodule;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.script.ScriptException;
import nevvermind.magewizards.utils.StringUtils;
import nevvermind.magewizards.utils.files.Freemarker;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Utilities;

public class FilesCreator
{
    private static final String TPL_MODULE_DEF = "Templates/Mage Wizards/New Module/module_definition.xml";
    private static final String TPL_MODULE_CONFIG = "Templates/Mage Wizards/New Module/config.xml";
    private static final String TPL_MODULE_DATA_HELPER = "Templates/Mage Wizards/New Module/Data.php";
    private static final String TPL_MODULE_INSTALLER_DATA = "Templates/Mage Wizards/New Module/sql-install.php";
    private static final String TPL_MODULE_INSTALLER_SQL = "Templates/Mage Wizards/New Module/data-install.php";
    private static final String TPL_MODULE_SETUP = "Templates/Mage Wizards/New Module/Setup.php";

    public static final String DEFAULT_MODULE_VERSION = "0.1.0";

    private Project project;
    private String namespace;
    private String name;
    private String isActive;
    private String baseInstallerClass;
    private Boolean makeDataInstall = false;
    private Boolean makeSqlInstall = false;

    public String getNamespace()
    {
        return namespace;
    }

    public void setNamespace(String namespace)
    {
        this.namespace = namespace;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getIsActive()
    {
        return isActive;
    }

    public void setIsActive(String isActive)
    {
        this.isActive = isActive.toLowerCase();
    }

    public String getModuleVersion()
    {
        return DEFAULT_MODULE_VERSION;
    }

    /**
     * @return String|null
     */
    public String getBaseInstallerClass()
    {
        return this.baseInstallerClass;
    }

    public void setBaseInstallerClass(String installerClass)
    {
        this.baseInstallerClass = installerClass;
    }

    public Boolean isWithInstall()
    {
        return isMakeDataInstall() || isMakeSqlInstall();
    }

    public Boolean isMakeDataInstall()
    {
        return makeDataInstall;
    }

    public void setMakeDataInstall(Boolean makeDataInstall)
    {
        this.makeDataInstall = makeDataInstall;
    }

    public Boolean isMakeSqlInstall()
    {
        return makeSqlInstall;
    }

    public void setMakeSqlInstall(Boolean makeSqlInstall)
    {
        this.makeSqlInstall = makeSqlInstall;
    }

    public Project getProject()
    {
        if (project == null) {
            project = Utilities.actionsGlobalContext().lookup(Project.class);
        }
        return project;
    }

    public void createFiles()
    {
        Map<String, Object> tplData = new HashMap<String, Object>();
        tplData.put("namespace", getNamespace());
        tplData.put("name", getName());
        tplData.put("active", getIsActive());
        tplData.put("version", getModuleVersion());
        tplData.put("withResource", isWithInstall());
        
        if (isWithInstall()) {
            tplData.put("installerClass", getBaseInstallerClass());
        }
        
        try {
            Freemarker.createFileFromTemplate(FileUtil.getConfigFile(TPL_MODULE_DEF), new File(getProjectRootPath() + getModuleDefinitionPath()), tplData);
            Freemarker.createFileFromTemplate(FileUtil.getConfigFile(TPL_MODULE_DATA_HELPER), new File(getProjectRootPath() + getDataHelperPath()), tplData);
            Freemarker.createFileFromTemplate(FileUtil.getConfigFile(TPL_MODULE_CONFIG), new File(getProjectRootPath() + getEtcConfigPath()), tplData);

            if (isWithInstall()) {
                Freemarker.createFileFromTemplate(FileUtil.getConfigFile(TPL_MODULE_SETUP), new File(getProjectRootPath() + getSetupInstallerPath()), tplData);
                
                if (isMakeDataInstall()) {
                    Freemarker.createFileFromTemplate(FileUtil.getConfigFile(TPL_MODULE_INSTALLER_DATA), new File(getProjectRootPath() + getDataInstallerPath()), tplData);
                }
                
                if (isMakeSqlInstall()) {
                    Freemarker.createFileFromTemplate(FileUtil.getConfigFile(TPL_MODULE_INSTALLER_SQL), new File(getProjectRootPath() + getSqlInstallerPath()), tplData);
                }
            }

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        } catch (ScriptException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    protected String getProjectRootPath()
    {
        return FileUtil.toFile(getProject().getProjectDirectory()).getPath();
    }

    protected String getModuleDefinitionPath()
    {
        return "/app/etc/modules/" + StringUtils.asTitle(getNamespace()) + "_" + StringUtils.asTitle(getName()) + ".xml";
    }

    protected String getModulePath()
    {
        return "/app/code/local/" + StringUtils.asTitle(getNamespace()) + "/" + StringUtils.asTitle(getName());
    }

    protected String getEtcConfigPath()
    {
        return getModulePath() + "/etc/config.xml";
    }

    protected String getDataHelperPath()
    {
        return getModulePath() + "/Helper/Data.php";
    }

    protected String getDataInstallerPath()
    {
        return getModulePath() + "/data/" + getNamespace().toLowerCase() + "_" + getName().toLowerCase() + "_setup/data-install-" + getModuleVersion() + ".php";
    }

    protected String getSqlInstallerPath()
    {
        return getModulePath() + "/sql/" + getNamespace().toLowerCase() + "_" + getName().toLowerCase() + "_setup/install-" + getModuleVersion() + ".php";
    }
    
    protected String getSetupInstallerPath()
    {
        return getModulePath() + "/Model/Resource/Setup.php";
    }    

    public List<String> getAllFiles()
    {
        List<String> fileList = new ArrayList<String>();

        fileList.add(getModuleDefinitionPath().replaceFirst("/", ""));
        fileList.add(getEtcConfigPath().replaceFirst("/", ""));
        fileList.add(getDataHelperPath().replaceFirst("/", ""));
        
        if (isWithInstall()) {
            fileList.add(getSetupInstallerPath().replaceFirst("/", ""));
        }

        if (isMakeDataInstall()) {
            fileList.add(getDataInstallerPath().replaceFirst("/", ""));
        }
        
        if (isMakeSqlInstall()) {
            fileList.add(getSqlInstallerPath().replaceFirst("/", ""));
        }

        return fileList;
    }
}
