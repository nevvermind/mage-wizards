<?xml version="1.0"?>
<config>
    <modules>
        <${namespace?cap_first}_${name?cap_first}>
            <active>${active}</active>
            <codePool>local</codePool>
            <depends />
        </${namespace?cap_first}_${name?cap_first}>
    </modules>
</config>