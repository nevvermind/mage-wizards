/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.utils;

import java.util.Arrays;
import java.util.Iterator;

public class StringUtils
{
    public static String asTitle(String str)
    {
        return Character.toUpperCase(str.charAt(0)) + str.substring(1);
    }
    
    public static boolean isNotBlank(String string)
    {
        return string != null && !string.equals("");
    }
    
    public static boolean isBlank(String string)
    {
        return !isNotBlank(string);
    }
    
    public static String join(String[] parts, String delimiter)
    {
        if (parts.length == 0) {
            return "";
        }
        
        StringBuilder result = new StringBuilder();
        Iterator<String> it = Arrays.asList(parts).iterator();
        
        while (it.hasNext()) {
            String part = it.next();
            result.append(part);
            if (it.hasNext()) {
                result.append(delimiter);
            }
        }
        
        return result.toString();
    }
    
    public static String removeNewlines(String string)
    {
        return string.replace(System.getProperty("line.separator"), "");
    }
}
