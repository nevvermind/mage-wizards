/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.utils.files;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

public class Freemarker 
{
    public static Boolean createFileFromTemplate(FileObject sourceTemplateFo, File destinationFile, Map<? extends String, ? extends Object> bindings) throws IOException, ScriptException
    {
        ScriptEngineManager scriptManager = new ScriptEngineManager();
        ScriptEngine freemarkerEngine = scriptManager.getEngineByName("freemarker");
        
        assert freemarkerEngine != null;
        
        ScriptContext ctx = freemarkerEngine.getContext();
        
        ctx.setAttribute(FileObject.class.getName(), sourceTemplateFo, ScriptContext.ENGINE_SCOPE);
        ctx.getBindings(ScriptContext.ENGINE_SCOPE).putAll(bindings);
        
        // file must be created before writing to it
        FileUtil.createData(FileUtil.normalizeFile(destinationFile));
        
        FileWriter writer = new FileWriter(destinationFile);
        ctx.setWriter(writer);            

        InputStreamReader is = new InputStreamReader(sourceTemplateFo.getInputStream());
        freemarkerEngine.eval(is);

        is.close();
        writer.close();
        
        return true;
    }
}
