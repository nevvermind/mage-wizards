/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.ui.swing;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;
import javax.swing.text.PlainDocument;

/**
 * <p>Decorator factory of JTextComponent objects which limits their <code>Document</code> length to the
 * specified limit.</p>
 * <p>The public inner <code>LimitDocument</code> class can be queried for the limit applied originally by the caller.</p>
 * <p>Of course, this changes the Component's <code>Document</code> model.</p>
 */
public class TextComponentLimiterDecorator
{
    /**
     * @param <T>
     * @param textComponent
     * @param limit Must be a positive integer
     * @return The passed object, with the Document model changed.
     */
    public static <T extends JTextComponent> T applyLimit(final T textComponent, int limit)
    {
        if (limit < 0) {
            throw new IllegalArgumentException("Limit should be positive.");
        }
        textComponent.setDocument(new LimitDocument(limit));
        return textComponent;
    }
    
    public final static class LimitDocument extends PlainDocument 
    {
        private final int limit;
        
        public LimitDocument(int limit)
        {
            this.limit = limit;
        }
        
        public int getLimit()
        {
            return limit;
        }

        @Override
        public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException {
            if (str == null) return;

            if ((getLength() + str.length()) <= limit) {
                super.insertString(offset, str, attr);
            }
        }         
    }
}
