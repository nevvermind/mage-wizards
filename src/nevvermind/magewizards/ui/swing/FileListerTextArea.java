/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.ui.swing;

import java.io.File;
import java.util.LinkedHashSet;
import java.util.Set;
import nevvermind.magewizards.utils.StringUtils;

public class FileListerTextArea extends javax.swing.JTextArea
{
    private final Set<File> files = new LinkedHashSet<File>();

    public final Set<File> getFiles()
    {
        return files;
    }
    
    public boolean addFile(String filePath)
    {
        if (StringUtils.isNotBlank(filePath)) {
            getFiles().add(new File(filePath));
            return true;
        }
        return false;
    }
    
    public final boolean setFileListToText()
    {
        if (getFiles().isEmpty()) {
            return false;
        }
        
        StringBuilder text = new StringBuilder();
        
        for (File file : getFiles()) {
            text.append(getFileLine(file)).append("\n");
        }
        
        this.setText(text.toString());
        
        getFiles().clear();
        
        return true;
    }
    
    protected String getFileLine(File file)
    {
        return fileExists(file) ? "(exists) " + file.getPath() : file.getPath();
    }
    
    private boolean fileExists(File file)
    {
        return file.exists();
    }
}
