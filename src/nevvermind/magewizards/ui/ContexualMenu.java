/**
 * The MIT License (MIT)
 * 
 * Copyright (c) 2014 Adrian Drăguş <giadiireg@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package nevvermind.magewizards.ui;

import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.Iterator;
import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import nevvermind.magewizards.api.ContextualAction;
import org.netbeans.api.project.Project;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.util.actions.Presenter;

@ActionID(category = "Project", id = "nevvermind.magewizards.ui.ContexualMenu")
@ActionRegistration(displayName = "#CTL_MenuLabel", lazy = false)
@ActionReference(path = "Projects/org-netbeans-modules-php-phpproject/Actions", position = 1000)
public class ContexualMenu extends AbstractAction implements Presenter.Popup
{
    private Project projectContext;
    
    public ContexualMenu() {
        super();
    }    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        assert false;
    }

    public Project getProjectContext()
    {
        return projectContext;
    }

    public void setProjectContext(Project projectContext)
    {
        this.projectContext = projectContext;
    }

    @Override
    public JMenuItem getPopupPresenter() {
        
        setProjectContext(Utilities.actionsGlobalContext().lookup(Project.class));
        return new MageWizardActions();
    }

    private final class MageWizardActions extends JMenu {

        @NbBundle.Messages("CTL_MenuLabel.name=Mage Wizards")
        public MageWizardActions() {
            super(Bundle.CTL_MenuLabel_name());
            
            Collection<? extends ContextualAction> allActionMenuItems = Lookup.getDefault().lookupAll(ContextualAction.class);
            
            Iterator<? extends ContextualAction> it = allActionMenuItems.iterator();
            
            while (it.hasNext()) {
                ContextualAction actionItem = it.next();
                actionItem.init(getProjectContext());
                add(actionItem);
                if (it.hasNext() && actionItem.addSeparatorAfter()) {
                    addSeparator();
                }
            }
        }

    }
}

